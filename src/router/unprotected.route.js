
/** ✅ Husehold import ✅ **/
import React from "react";
import { Route } from 'react-router-dom'

/** 💰 Store 💰 **/
import { connect } from 'react-redux';


// import login from store
import { getLoginError, getLoginUser, getLoginStatus } from '../redux/login/login.reducer'

import Header from "../containers/header/ecommerce.header"
import Footer from "../containers/footer/footer"

//const RouteAuth = ({ component: Component, ...rest }) => {
class RouteAuth extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        const Component = this.props.component
        return (
            <React.Fragment>
                <Header />
                <Route
                    render={props => {
                        return <Component {...props} />
                    }
                    } />
                <Footer />
            </React.Fragment>

        )
    }

}

const mapStateToProps = (state /**, ownProps */) => {
    return {
        error: getLoginError(state.auth),
        pending: getLoginStatus(state.auth),
        user: getLoginUser(state.auth)
    }
}


export default connect(
    mapStateToProps,  //subscribing to the store by mapping store values in our component's props || null
    null //inject action creators in our component's props
)(RouteAuth)