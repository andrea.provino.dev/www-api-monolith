/** 🛁 HOUSEHOLD related stuff */
import { BrowserRouter, Switch, Redirect } from 'react-router-dom'
import React from "react"
import Home from '../containers/home/home'
import Login from '../containers/login/login'
import Signup from "../containers/signup/signup"
import Landing from "../containers/landing/landing"
import { Provider } from 'react-redux'
import store from "../redux/store/store"


/** 👨‍👨‍👦 USER related stuff */
import UserList from '../components/user/userList'

/** 📦 PRODUCT related stuff */
import ProductList from "../components/products/product.all.dashboard"
import ProductHandle from "../components/products/product.handle"
import Cart from "../containers/buy/cart"

/**  🔬 UTILS HOC related stuff */
import Dashboard from './protected.route'
import ECommerce from "./unprotected.route"

/** 💰 TRANSACTION related stuff */
import TransactionList from "../components/transactions/transactions.all"

/** COURSE */
import Course from "../components/course/course"
import Section from "../components/section/section"

// define history adn access it everywhere
import { createBrowserHistory } from 'history';
export const history = createBrowserHistory();

export const routing = (
    <Provider store={store}>
        <BrowserRouter history={history}>

            <Switch>

                <ECommerce exact path="/" component={Landing} />
                <ECommerce path="/login" component={Login} />
                <ECommerce path="/signup" component={Signup} />
                <ECommerce path="/success" component={Login} />
                <Dashboard path="/classroom" component={Course} />
                <Dashboard path="/section" component={Section} />

                <ECommerce path="/home" component={Home} />
                <ECommerce path="/cart" component={Cart} />
                <Dashboard path="/dashboard/users" component={UserList} />
                <Dashboard path="/dashboard/product/all" component={ProductList} />
                <Dashboard path="/dashboard/product/add" component={ProductHandle} />
                <Dashboard path="/dashboard/product/edit/:productId" component={ProductHandle} />
                <Dashboard path="/dashboard/transactions/all" component={TransactionList} />
                <Dashboard path="/dashboard/purchases/all" component={TransactionList} />
                <Redirect to="/" />

            </Switch>

        </BrowserRouter>
    </Provider>
)