
/** ✅ Husehold import ✅ **/
import React from "react";
import { Route, Redirect } from 'react-router-dom'

/** 💰 Store 💰 **/
import { connect } from 'react-redux';


// import login from store
import { getLoginError, getLoginUser, getLoginStatus } from '../redux/login/login.reducer'

// import component
import NavBar from "../components/navbar/dashboard.navbar"
import AdminSidebar from "../components/sidebar/admin.sidebar"
import UserSidebar from "../components/sidebar/user.sidebar"

// import css
import "../components/sidebar/sidebar.css"

class RouteAuth extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            sideNavToggled: true
        }
        this.toggleSideNav = this.toggleSideNav.bind(this)
    }
    toggleSideNav = () => {
        this.setState({
            ...this.state,
            sideNavToggled: !this.state.sideNavToggled
        })
    }
    render() {
        const Component = this.props.component
        return (
            <React.Fragment>
                <NavBar onSideNavToggleClick={this.toggleSideNav} />
                <Route
                    render={props => {
                        const { user } = this.props
                        if (user) {
                            switch (user.role) {
                                case "admin":
                                    return (
                                        <div className="dashboard-working-area">

                                            <AdminSidebar test="cipolla" triggerOpening={this.state.sideNavToggled} />
                                            <Component {...props} />
                                        </div>
                                    )
                                case "user":
                                    return (
                                        <div className="dashboard-working-area">
                                            <UserSidebar test="cipolla" triggerOpening={this.state.sideNavToggled} />
                                            <Component {...props} />
                                        </div>
                                    )
                                default:
                                    return (
                                        <div className="dashboard-working-area">
                                            <p>User do not recognised: ERROR_SWITCH_CASE_KEY</p>
                                        </div>
                                    )
                            }
                        } else {
                            return <Redirect to={{
                                pathname: "/login",
                                state: {
                                    from: props.location
                                }
                            }} />
                        }
                    }} />
            </React.Fragment>

        )
    }

}

const mapStateToProps = (state /**, ownProps */) => {
    return {
        error: getLoginError(state.auth),
        pending: getLoginStatus(state.auth),
        user: getLoginUser(state.auth)
    }
}


export default connect(
    mapStateToProps,  //subscribing to the store by mapping store values in our component's props || null
    null //inject action creators in our component's props
)(RouteAuth)