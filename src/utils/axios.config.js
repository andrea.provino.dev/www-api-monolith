import axios from 'axios'

import store from '../redux/store/store'
import { refreshToken } from '../redux/login/login.store';
const https = axios.create({
    baseURL: process.env.REACT_APP_BASE_URL,
})

https.interceptors.request.use((config) => {
    const token_raw = localStorage.getItem('accessToken')
    const token = JSON.parse(token_raw)
    //add bearer
    if (token) config.headers.authorization = "Bearer " + token;
    return config;
}, (error) => {
    return Promise.reject(error);
});

https.interceptors.response.use((response) => {
    if (response.status === 401) {
        //logout()
    }
    return response;
}, (error) => {
    if (error.response.status === 401) {
        // get auth store state
        const { auth } = store.getState()
        // create payload for refreshtoken
        const payload = {
            refreshToken: auth.refreshToken,
            email: auth.user.email
        }
        store.dispatch(refreshToken(payload))
    }
    if (error.response && error.response.data) {
        return Promise.reject(error.response.data);
    }
    return Promise.reject(error.message);
});

export default https
