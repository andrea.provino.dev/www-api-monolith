export const LOGIN_PENDING = 'LOGIN_PENDING'
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS'
export const LOGIN_ERROR = 'LOGIN_ERROR'
export const LOGOUT = 'LOGOUT'

export function loginPending() {
    return {
        type: LOGIN_PENDING
    }
}

export function loginSuccess(user) {
    return {
        type: LOGIN_SUCCESS,
        payload: user
    }
}

export function loginError(error) {
    return {
        type: LOGIN_ERROR,
        error: error
    }
}

export function logOut() {
    return {
        type: LOGOUT
    }
}