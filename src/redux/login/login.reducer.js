
// import actions code
import { LOGIN_ERROR, LOGIN_PENDING, LOGIN_SUCCESS, LOGOUT } from './login.action'

const initialState = {
    pending: false,
    user: JSON.parse(localStorage.getItem('user')) || null,
    error: null,
    accessToken: JSON.parse(localStorage.getItem('accessToken')) || null,
    refreshToken: JSON.parse(localStorage.getItem('refreshToken')) || null,
}

const loginReducer = (state = initialState, action) => {

    switch (action.type) {
        case LOGIN_PENDING:
            return {
                ...state,
                pending: true
            }
        case LOGIN_SUCCESS:
            return {
                ...state,
                pending: false,
                user: action.payload.user,
                accessToken: action.payload.accessToken,
                refreshToken: action.payload.refreshToken,
            }
        case LOGIN_ERROR:
            return {
                ...state,
                pending: false,
                error: action.error
            }
        case LOGOUT:
            return {
                initialState
            }
        default:
            return state
    }
}

const getLoginUser = state => state.user
const getLoginStatus = state => state.pending
const getLoginError = state => state.error

export {
    loginReducer,
    getLoginUser,
    getLoginStatus,
    getLoginError
}