// import actions
import { loginError, loginSuccess, loginPending, logOut } from './login.action'
import https from '../../utils/axios.config'

const login = (credential, history) => {
    return (dispatch) => {
        // set status pending
        dispatch(loginPending())
        https.post("/users/log-in", credential)
            .then(res => {
                // extract token and user data
                const access_token_payload = res.data.result.accessToken.payload
                const user_role = res.data.result.accessToken.userRole
                const accessToken = res.data.result.accessToken.jwtToken
                const refreshToken = res.data.result.refreshToken.token
                const User = {
                    id: access_token_payload.sub,
                    role: user_role.name,
                    role_id: user_role._id,
                    email: access_token_payload.username
                }
                // store it in case of refresh
                localStorage.setItem('accessToken', JSON.stringify(accessToken))
                localStorage.setItem('refreshToken', JSON.stringify(refreshToken))
                localStorage.setItem('user', JSON.stringify(User))
                // save to redux store
                const payload = {
                    user: User,
                    accessToken: accessToken,
                    refreshToken: refreshToken
                }
                dispatch(loginSuccess(payload))
                debugger;
                history.push('/classroom')
            })
            .catch(err => {
                dispatch(loginError(err.error.message))
            })
    }
}
const logout = () => {
    return dispatch => {
        localStorage.removeItem("refreshToken")
        localStorage.removeItem("accessToken")
        localStorage.removeItem("user")
        dispatch(logOut())
    }
}
const refreshToken = (auth_session_params) => {
    return dispatch => {
        debugger;
        dispatch(loginPending())
        https.post("/users/refresh-token", auth_session_params)
            .then(res => {
                const access_token_payload = res.data.result.accessToken.payload
                const user_role = res.data.result.accessToken.userRole
                const accessToken = res.data.result.accessToken.jwtToken
                const refreshToken = res.data.result.refreshToken.token
                const User = {
                    id: access_token_payload.sub,
                    role: user_role.name,
                    role_id: user_role._id,
                    email: access_token_payload.username
                }
                // store it in case of refresh
                localStorage.setItem('accessToken', JSON.stringify(accessToken))
                localStorage.setItem('refreshToken', JSON.stringify(refreshToken))
                localStorage.setItem('user', JSON.stringify(User))
                // save to redux store
                const payload = {
                    user: User,
                    accessToken: accessToken,
                    refreshToken: refreshToken
                }
                dispatch(loginSuccess(payload))
            })
            .catch(err => {
                dispatch(logout())
                console.log(err)
                //dispatch(loginError(err))
            })
    }
}
export {
    login,
    logout,
    refreshToken
}