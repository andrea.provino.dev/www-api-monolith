import actions from "./transactions.action"

const initial_state = {
    pending: false,
    error: null,
    transactions: []
}

const transactionsReducer = (state = initial_state, action) => {
    switch (action.type) {
        case actions.TRX_PENDING:
            return {
                ...state,
                pending: true
            }
        case actions.TRX_SUCCESS:
            return {
                ...state,
                pending: false,
                transactions: action.trxs
            }
        case actions.TRX_SUCCESS:
            return {
                ...state,
                pending: false,
                error: action.error
            }
        default: return state
    }
}

const getTransactionsError = state => state.error
const getTransactions = state => state.transactions
const getTransactionsStatus = state => state.pending

export {
    transactionsReducer,
    getTransactionsError,
    getTransactions,
    getTransactionsStatus
}



// session ID: cs_test_yGo2Wuz4y7jUP1gUsKyvbvnlKDr1uZPTlNdyWgKT3RjdL2oEJRD7KYD1
// trx ID: 5dbd6cf8e047441d4020a792