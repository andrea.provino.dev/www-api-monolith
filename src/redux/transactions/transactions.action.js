const TRX_PENDING = 'TRX_PENDING'
const TRX_SUCCESS = 'TRX_SUCCESS'
const TRX_ERROR = 'TRX_ERROR'

function trxPending() {
    return {
        type: TRX_PENDING
    }
}

function trxSuccess(payload) {
    return {
        type: TRX_SUCCESS,
        trxs: payload
    }
}

function trxError(error) {
    return {
        type: TRX_ERROR,
        error: error
    }
}

export default {
    TRX_ERROR,
    TRX_SUCCESS,
    TRX_PENDING,

    trxPending,
    trxSuccess,
    trxError
}