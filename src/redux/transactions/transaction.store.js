import actions from "../transactions/transactions.action"
import https from "../../utils/axios.config"

const getTransactions = () => {
    return dispatch => {
        dispatch(actions.trxPending())
        https.get("/transactions")
            .then(res => {
                dispatch(actions.trxSuccess(res.data.transactions))
            })
            .catch(err => {
                dispatch(actions.trxError(err))
            })
    }
}

/**
 * Update Stripe Session Id From Transaction Id
 * 
 * Add Stripe Session Id to existing Transaction by passing following params
 * 
 * @param {*} session_id 
 * @param {*} trx_id 
 */
const updateStripeSessionid = (session_id, trx_id) => {
    return dispatch => {
        dispatch(actions.trxPending())
        const payload = {
            stripe_session_id: session_id
        }
        https.post(`/transaction/${trx_id}/updateStripeSession`, payload)
            .then(transaction => {
                // dispatch(actions.trxSuccess(transactions))
                console.log(transaction)
            })
            .catch(err => {
                dispatch(actions.trxError(err))
            })
    }
}

export default {
    getTransactions,
    updateStripeSessionid
}