
import actions from './users.action'
import https from "../../utils/axios.config"

const getUsers = () => {
    return dispatch => {
        dispatch(actions.usersPending())
        https.get("/users")
            .then(res => {
                dispatch(actions.usersList(res.data))
            })
            .catch(err => {
                dispatch(actions.usersError(err))
            })
    }
}

export {
    getUsers
}