
import actions from "./users.action"

const initialState = {
    pending: null,
    error: null,
    users: [],
}

const usersReducer = (state = initialState, action) => {
    switch (action.type) {
        case actions.USERS_PENDING:
            return {
                ...state,
                pending: true
            }
        case actions.USERS_LIST:
            return {
                ...state,
                pending: false,
                users: action.users
            }
        case actions.USERS_PENDING:
            return {
                ...state,
                pending: false,
                error: action.error
            }
        default:
            return state
    }
}

const getUsersStatus = state => state.pending
const getUsersList = state => state.users
const getUsersError = state => state.error
export {
    usersReducer,
    getUsersStatus,
    getUsersList,
    getUsersError
}