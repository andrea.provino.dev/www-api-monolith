const USERS_LIST = "USERS_LIST"
const USERS_ERROR = "USERS_ERROR"
const USERS_PENDING = "USERS_PENDING"

const usersPending = () => {
    return {
        type: USERS_PENDING
    }
}
const usersError = (error) => {
    return {
        type: USERS_ERROR,
        error: error
    }
}
const usersList = (payload) => {
    return {
        type: USERS_LIST,
        users: payload
    }
}

export default {
    USERS_LIST,
    USERS_ERROR,
    USERS_PENDING,
    
    usersPending,
    usersError,
    usersList
}