
import actions from "../products/products.action"
import https from "../../utils/axios.config"

const getProducts = () => {
    return (dispatch) => {
        dispatch(actions.productPending())
        https.get("/products")
            .then(res => {
                dispatch(actions.productSuccess(res.data))
            })
            .catch(err => {
                dispatch(actions.productError(err))
            })
    }
}

const getProductById = (product_id) => {
    return (dispatch) => {
        dispatch(actions.productPending())
        https.get("/products/" + product_id)
            .then(res => {
                dispatch(actions.productSuccess(res.data))
            })
            .catch(err => {
                dispatch(actions.productError(err))
            })
    }
}

export default {
    getProducts,
    getProductById
}