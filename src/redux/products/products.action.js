const PRODUCT_PENDING = 'PRODUCT_PENDING'
const PRODUCT_SUCCESS = 'PRODUCT_SUCCESS'
const PRODUCT_ERROR = 'PRODUCT_ERROR'

function productPending() {
    return {
        type: PRODUCT_PENDING
    }
}

function productSuccess(payload) {
    return {
        type: PRODUCT_SUCCESS,
        products: payload
    }
}

function productError(error) {
    return {
        type: PRODUCT_ERROR,
        error: error
    }
}

export default {
    PRODUCT_ERROR,
    PRODUCT_SUCCESS,
    PRODUCT_PENDING,

    productPending,
    productSuccess,
    productError
}