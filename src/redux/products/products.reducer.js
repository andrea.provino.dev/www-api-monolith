import actions from "./products.action"

const initial_state = {
    error: null,
    pending: false,
    products: []
}


/** ⚠️ 
 * actions = list of available code or action maker
 * action️ = single action passed to the reducer
 * ⚠️ */
const productsReducer = (state = initial_state, action) => {
    switch (action.type) {
        case actions.PRODUCT_PENDING:
            return {
                ...state,
                pending: true,
            }
        case actions.PRODUCT_SUCCESS:
            return {
                ...state,
                pending: false,
                products: action.products,
            }
        case actions.PRODUCT_:
            return {
                ...state,
                pending: false,
                error: action.error
            }
        default:
            return state
    }
}

const getProductsError = state => state.error
const getProducts = state => state.products
const getProductsStatus = state => state.pending

export {
    productsReducer,
    getProductsError,
    getProducts,
    getProductsStatus
}