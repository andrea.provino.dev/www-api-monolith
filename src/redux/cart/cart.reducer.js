import actions from "./cart.action"

const initialState = {
    pending: false,
    items: [],
    error: null,
    check_out_error: null,
    check_out_success: null
}
/** ⚠️
 * actions = list of available code or action maker
 * action️ = single action passed to the reducer
 * ⚠️ */
let old_items = []
const cartReducer = (state = initialState, action) => {
    switch (action.type) {
        case actions.CART_PENDING:
            return {
                ...state,
                pending: true,
            }
        case actions.CART_ADD_ITEM:
            // TODO move this code into store (pure function error)
            old_items = [...state.items]
            let new_item = {}
            // check if item is already present
            let item_found = old_items.filter(item => item._id === action.item._id)
            if (!item_found.length) {
                // item not present, add one with qty 1
                new_item = action.item
                new_item['qty'] = 1
            } else {
                // item present, simply increase qty
                item_found = item_found[0]
                item_found.qty += 1
                new_item = item_found
            }
            // get all items but the one added
            let new_items = old_items.filter(item => item._id !== action.item._id)
            // add new item
            new_items.push(new_item)
            /**
             * Search for duplicate, if found sum them to get 
             * the number of duplicate items.
             * 
             * The 'qty' key is added by the following function, so 
             * it will be only present afte the first run. In that case
             * instead of checking in the 'acc' object, it add 1 to current 
             * number of items 
             *   const duplicated_items = new_items.reduce((acc, curr) => {
                debugger;
                const id = curr._id
                acc[id] = !curr.qty ? 1 : curr.qty + 1
                return acc
            }, {})
            const cleaned_items = Object.keys(duplicated_items).map(id => {
                let raw_item = new_items.filter(sub_item => sub_item._id === id)
                raw_item = raw_item[0]
                raw_item["qty"] = duplicated_items[id] // piece of shit
                return raw_item
            })
             */
            return {
                ...state,
                pending: false,
                items: new_items
            }
        case actions.CART_REMOVE_ITEM:
            old_items = [...state.items]
            const filtered_items = old_items.filter(item => item !== action.item)
            return {
                ...state,
                pending: false,
                items: filtered_items
            }
        case actions.CART_REMOVE_ALL:
            return {
                ...state,
                pending: false,
                items: []
            }
        case actions.CHECKOUT_ERROR:
            return {
                ...state,
                pending: false,
                check_out_error: action.errror
            }
        case actions.CHECKOUT_SUCCESS:
            return {
                ...state,
                pending: false,
                check_out_success: action.payload
            }
        case actions.CART_ERROR:
            return {
                ...state,
                pending: false,
                error: action.error
            }
        default:
            return state
    }
}
const getCartItems = (state) => state.items
const getCartStatus = (state) => state.pending
const getCartError = (state) => state.error
const getCheckoutError = (state) => state.check_out_error
const getCheckoutSuccess = (state) => state.check_out_success
export {
    cartReducer,
    getCartItems,
    getCartStatus,
    getCartError,
    getCheckoutError,
    getCheckoutSuccess
}