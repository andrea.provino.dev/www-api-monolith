const CART_PENDING = "CART_PENDING"
const CART_ERROR = "CART_ERROR"
const CART_ADD_ITEM = "CART_ADD_ITEM"
const CART_REMOVE_ITEM = "CART_REMOVE_ITEM"
const CART_REMOVE_ALL = "CART_REMOVE_ALL"
const CHECKOUT_ERROR = "CHECKOUT_ERROR"
const CHECKOUT_SUCCESS = "CHECKOUT_SUCCESS"
const cartPending = () => {
    return {
        type: CART_PENDING
    }
}

const cartAddItem = (payload) => {
    return {
        type: CART_ADD_ITEM,
        item: payload
    }
}

const cartRemoveItem = (payload) => {
    return {
        type: CART_REMOVE_ITEM,
        item: payload
    }
}

const cartRemoveAll = () => {
    return {
        type: CART_REMOVE_ALL
    }
}

const cartError = (error) => {
    return {
        type: CART_ERROR,
        error: error
    }
}
const checkoutError = (error) => {
    return {
        type: CHECKOUT_ERROR,
        error: error
    }
}
const checkoutSuccess = (payload) => {
    return {
        type: CHECKOUT_SUCCESS,
        payload: payload
    }
}



export default {
    cartPending,
    cartAddItem,
    cartRemoveItem,
    cartError,
    cartRemoveAll,
    checkoutError,
    checkoutSuccess,

    CART_PENDING,
    CART_ERROR,
    CART_ADD_ITEM,
    CART_REMOVE_ITEM,
    CART_REMOVE_ALL,
    CHECKOUT_SUCCESS,
    CHECKOUT_ERROR
}