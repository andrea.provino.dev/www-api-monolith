import actions from "./cart.action"
import https from "../../utils/axios.config"
const addItem = (item) => {
    return dispatch => {
        dispatch(actions.cartPending())
        dispatch(actions.cartAddItem(item))
    }
}

const removeItem = (item) => {
    return dispatch => {
        dispatch(actions.cartPending())
        dispatch(actions.cartRemoveItem(item))
    }

}
const removeAll = () => {
    return dispatch => {
        dispatch(actions.cartPending())
        dispatch(actions.cartRemoveAll())
    }
}
const checkOut = (this_payload, this_stripe) => {
    const that_payload = this_payload
    const that_stripe = this_stripe
    return dispatch => {
        dispatch(actions.cartPending())
        // create payload, only with id and qty
        const payload_items = that_payload.items.map(item => {
            return {
                id: item._id,
                qty: item.qty
            }
        })
        const payload = {
            //is_billing_address_required: false,
            //client_id: that_payload.user.id,
            username: that_payload.user,
            password: that_payload.password,
            currency: "usd",
            courseId: "5e388495c7fbaf045cd0cfad"
        }
        return https.post('/checkout/course', payload).then(res => {
            dispatch(actions.checkoutSuccess(res.data))
            debugger;
            try {
                that_stripe.redirectToCheckout({
                    // Make the id field from the Checkout Session creation API response
                    // available to this file, so you can provide it as parameter here
                    // instead of the {{CHECKOUT_SESSION_ID}} placeholder.
                    sessionId: res.data.id
                }).then(res => {
                    debugger;
                    console.log(res)
                    localStorage.setItem('stripe', JSON.stringify(res))
                })
            } catch (err) {
                console.log(err)
            }

        }).catch(err => {
            dispatch(actions.checkoutError(err))
        })
    }

}
export default {
    addItem,
    removeItem,
    removeAll,
    checkOut
}