
import { SIGNUP_ERROR, SIGNUP_PENDING, SIGNUP_SUCCESS, VERIFY_SUCCESS } from "./signup.action"

const initial_state = {
    error: null,
    message: null,
    pending: false,
    verify: null
}

const singupReducer = (state = initial_state, action) => {
    switch (action.type) {
        case SIGNUP_PENDING:
            return {
                ...state,
                pending: true
            }
        case SIGNUP_SUCCESS:
            return {
                ...state,
                pending: false,
                message: action.payload
            }
        case VERIFY_SUCCESS:
            return {
                ...state,
                pending: false,
                message: action.payload
            }
        case SIGNUP_ERROR:
            return {
                ...state,
                pending: false,
                error: action.error
            }
        default:
            return state
    }
}

const getSingupError = state => state.error
const getSingupMessage = state => state.message
const getSignUpStatus = state => state.pending
const getverifySuccess = state => state.verify

export {
    singupReducer,
    getSingupError,
    getSingupMessage,
    getSignUpStatus,
    getverifySuccess
}