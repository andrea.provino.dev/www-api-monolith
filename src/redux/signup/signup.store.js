import { signupError, signupSuccess, signupPending, verifySuccess } from './signup.action'
import https from '../../utils/axios.config'
import { history } from "../../router/index"

const signUp = (payload) => {
    return (dispatch) => {
        dispatch(signupPending())
        https.post('/users/sign-up', payload)
            .then(res => {
                const message = "Please, check your email and provide the verification code just sent to you."
                dispatch(signupSuccess(message))
            })
            .catch(err => {
                debugger;
                dispatch(signupError(err.response.data.error))
            })
    }
}
const verifyEmail = (payload) => {
    return (dispatch) => {
        dispatch(signupPending())
        https.post('/users/verify-email', payload)
            .then(res => {
                debugger;
                dispatch(verifySuccess(res.data.message))
                history.push('/dashboard/purchases/all');
            })
            .catch(err => {
                debugger;
                dispatch(signupError(err.response.data.error))
            })
    }
}
export {
    signUp,
    verifyEmail
}