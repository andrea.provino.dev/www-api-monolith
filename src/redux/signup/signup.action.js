export const SIGNUP_PENDING = 'SIGNUP_PENDING'
export const SIGNUP_SUCCESS = 'SIGNUP_SUCCESS'
export const SIGNUP_ERROR = 'SIGNUP_ERROR'
export const VERIFY_SUCCESS = 'VERIFY_SUCCESS'

export function signupPending() {
    return {
        type: SIGNUP_PENDING
    }
}

export function signupSuccess(payload) {
    return {
        type: SIGNUP_SUCCESS,
        payload: payload
    }
}

export function verifySuccess(payload) {
    return {
        type: VERIFY_SUCCESS,
        payload: payload
    }
}
export function signupError(error) {
    return {
        type: SIGNUP_ERROR,
        error: error
    }
}
