import actions from "./course.action"
import https from "../../utils/axios.config"

const getCourses = () => {
    return dispatch => {
        dispatch(actions.coursePending())
        https.get(`/courses`)
            .then(res => {
                dispatch(actions.courseSuccess(res.data))
            })
            .catch(err => {
                dispatch(actions.courseError(err))
            })
    }
}

const getCourseById = (courseId) => {
    return dispatch => {
        dispatch(actions.coursePending())
        https.get(`/course/${courseId}`)
            .then(res => {
                dispatch(actions.courseSuccess(res.data.courses))
            })
            .catch(err => {
                dispatch(actions.courseError(err))
            })
    }
}

export default {
    getCourses,
    getCourseById
}