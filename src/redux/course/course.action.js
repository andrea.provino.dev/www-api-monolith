const COURSE_PENDING = 'COURSE_PENDING'
const COURSE_SUCCESS = 'COURSE_SUCCESS'
const COURSE_ERROR = 'COURSE_ERROR'

function coursePending() {
    return {
        type: COURSE_PENDING
    }
}

function courseSuccess(payload) {
    return {
        type: COURSE_SUCCESS,
        courses: payload
    }
}

function courseError(error) {
    return {
        type: COURSE_ERROR,
        error: error
    }
}

export default {
    COURSE_ERROR,
    COURSE_SUCCESS,
    COURSE_PENDING,

    coursePending,
    courseSuccess,
    courseError
}