import actions from "./course.action"

const initial_state = {
    pending: false,
    error: null,
    courses: []
}

const courseReducer = (state = initial_state, action) => {
    switch (action.type) {
        case actions.COURSE_PENDING:
            return {
                ...state,
                pending: true
            }
        case actions.COURSE_SUCCESS:
            return {
                ...state,
                pending: false,
                courses: action.courses
            }
        case actions.COURSE_SUCCESS:
            return {
                ...state,
                pending: false,
                error: action.error
            }
        default: return state
    }
}

const getCoursesError = state => state.error
const getCourses = state => state.courses
const getCoursesStatus = state => state.pending

export {
    courseReducer,
    getCoursesError,
    getCourses,
    getCoursesStatus
}

