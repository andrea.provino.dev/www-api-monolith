
// in order to create the root reducer
import { combineReducers } from 'redux'
// import other reducers (functions)
import { loginReducer } from '../login/login.reducer'
import { singupReducer } from '../signup/signup.reducer'
import { productsReducer } from "../products/products.reducer"
import { usersReducer } from "../users/users.reducer"
import { cartReducer } from "../cart/cart.reducer"
import { transactionsReducer } from "../transactions/transactions.reducer"
import { courseReducer } from "../course/course.reducer"
import { moduleReducer } from "../module/module.reducer"
// the app state
const reducers = combineReducers({
    auth: loginReducer,
    signup: singupReducer,
    products: productsReducer,
    users: usersReducer,
    cart: cartReducer,
    transactions: transactionsReducer,
    course: courseReducer,
    module: moduleReducer
})

export default reducers