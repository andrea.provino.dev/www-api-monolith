import { createStore, compose, applyMiddleware } from 'redux'
import rootReducer from '../store/reducers'
import thunk from 'redux-thunk'

const middleware = [thunk]
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
const enhancer = composeEnhancers(applyMiddleware(...middleware))
export default createStore(rootReducer, {}, enhancer)