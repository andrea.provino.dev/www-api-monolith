import actions from "./module.action"

const initial_state = {
    pending: false,
    error: null,
    modules: []
}

const moduleReducer = (state = initial_state, action) => {
    switch (action.type) {
        case actions.MODULE_PENDING:
            return {
                ...state,
                pending: true
            }
        case actions.MODULE_SUCCESS:
            return {
                ...state,
                pending: false,
                modules: action.modules
            }
        case actions.MODULE_SUCCESS:
            return {
                ...state,
                pending: false,
                error: action.error
            }
        default: return state
    }
}

const getModulesError = state => state.error
const getModules = state => state.modules
const getModulesStatus = state => state.pending

export {
    moduleReducer,
    getModulesError,
    getModules,
    getModulesStatus
}

