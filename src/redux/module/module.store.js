import actions from "./module.action"
import https from "../../utils/axios.config"

const getModulesByCourseId = (courseId) => {
    return dispatch => {
        dispatch(actions.modulePending())
        https.get(`/course/${courseId}/modules`)
            .then(res => {
                dispatch(actions.moduleSuccess(res.data))
            })
            .catch(err => {
                dispatch(actions.moduleError(err))
            })
    }
}

export default {
    getModulesByCourseId
}