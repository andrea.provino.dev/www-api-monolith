const MODULE_PENDING = 'MODULE_PENDING'
const MODULE_SUCCESS = 'MODULE_SUCCESS'
const MODULE_ERROR = 'MODULE_ERROR'

function modulePending() {
    return {
        type: MODULE_PENDING
    }
}

function moduleSuccess(payload) {
    return {
        type: MODULE_SUCCESS,
        modules: payload
    }
}

function moduleError(error) {
    return {
        type: MODULE_ERROR,
        error: error
    }
}

export default {
    MODULE_ERROR,
    MODULE_SUCCESS,
    MODULE_PENDING,

    modulePending,
    moduleSuccess,
    moduleError
}