import ReactDOM from 'react-dom'
import 'element-theme-default';
import "bootstrap-css-only/css/bootstrap.min.css"
import "mdbreact/dist/css/mdb.css"
import '@fortawesome/fontawesome-free/css/all.min.css'
import './index.css'
import * as serviceWorker from './serviceWorker'
import { routing } from "./router/index"


ReactDOM.render(routing, document.getElementById('root'))

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
if (typeof window !== 'undefined') {
    console.log('client side rendering')
} else {
    console.log('server side rendering')
}
serviceWorker.unregister()
