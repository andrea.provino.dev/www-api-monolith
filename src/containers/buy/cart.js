import React from 'react'
import "../home/home"
import CartComponent from '../../components/cart/cart'
import { MDBContainer } from 'mdbreact'

class CartPage extends React.Component {
    render() {
        return (
            <MDBContainer>
                <div style={{
                    marginLeft: "8%",
                    marginRight: "8%"
                }} >
                    <CartComponent />
                </div>
            </MDBContainer>

        )
    }
}
export default CartPage