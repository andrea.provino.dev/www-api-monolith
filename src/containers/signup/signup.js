/** ✅ Husehold import ✅ **/
import React from 'react'
import SignupForm from '../../components/signup/signup.form'
import { MDBContainer } from "mdbreact"

class Signup extends React.Component {
    constructor(props) {
        super(props)
        this.state = {

        }
    }
    render() {
        return (
            <MDBContainer className="mt-4 p-4">
                <SignupForm />
            </MDBContainer>
        )
    }
}



export default Signup