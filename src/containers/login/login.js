/** ✅ Husehold import ✅ **/
import React from 'react'
import LoginForm from '../../components/login/login.form'
import { MDBContainer } from "mdbreact"

class Login extends React.Component {
    constructor(props) {
        super(props)
        this.state = {

        }
    }
    render() {
        return (
            <MDBContainer className="mt-4 p-4">
                <LoginForm />
            </MDBContainer>

        )
    }
}



export default Login