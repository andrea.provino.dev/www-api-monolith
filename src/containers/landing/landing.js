import React from 'react'
import axios from 'axios'
import { MDBContainer, MDBCollapse, MDBCard, MDBCardBody, MDBCollapseHeader, MDBIcon, MDBRow, MDBCol, MDBBox, MDBBtn, MDBInput } from "mdbreact";
import { Elements, StripeProvider, injectStripe } from 'react-stripe-elements';
import { Link } from 'react-router-dom'
// import css
import "./landing.css"

// import component
import Countdown from "../../components/countdown/countdown"

// import file json
import qa from "../../assets/q_a_copy.json"
import modules from "../../assets/modules_copy.json"

import { ReactComponent as Image2 } from "../../assets/Introduction.svg"
import { ReactComponent as Image3 } from "../../assets/Introduction-2.svg"
import { ReactComponent as Image1 } from "../../assets/Introduction-3.svg"
import { ReactComponent as Refund } from "../../assets/refund.svg"
import { ReactComponent as Image4 } from "../../assets/cross-validation.svg"
import { ReactComponent as Image5 } from "../../assets/underfitting-overfitting.svg"
import { ReactComponent as Image6 } from "../../assets/ensemble.svg"
import { ReactComponent as NextYear } from "../../assets/2021.svg"

import { ReactComponent as Blob1 } from "../../assets/blob-shape/1.svg"
import { ReactComponent as Blob2 } from "../../assets/blob-shape/2.svg"
import { ReactComponent as Blob3 } from "../../assets/blob-shape/3.svg"
import { ReactComponent as Blob4 } from "../../assets/blob-shape/4.svg"
import { ReactComponent as Blob5 } from "../../assets/blob-shape/5.svg"

import { ReactComponent as Enroll1 } from "../../assets/EDA.svg"
import { ReactComponent as Enroll2 } from "../../assets/skill-1.svg"
import { ReactComponent as Enroll3 } from "../../assets/skill-2.svg"
import { ReactComponent as Enroll4 } from "../../assets/skill-3.svg"
import { bindActionCreators } from "redux"
import { connect } from "react-redux"
import store from "../../redux/cart/cart.store"
const config = {
    cors: "https://cors-anywhere.herokuapp.com/",
    formUrl: "https://docs.google.com/forms/d/e/1FAIpQLSdBQIwDH9JYvqmp5ALbvardYEwyRYDe_I3h8cfZ2udCEou84Q/formResponse"
}
const formRaw = {
    email: { id: 1730842451, value: "" },
    payed: { id: 1808417347, value: false }
}
const stripe = {
    private_key: process.env.REACT_APP_PRIVATE_KEY,
    public_key: process.env.REACT_APP_PUBLIC_KEY
}
class _CheckOutButton extends React.Component {
    constructor(props) {
        super(props)

    }
    handleSubmit = async () => {
        const payload = {
            user: this.props.email,
            password: this.props.password,
            items: [{
                qty: 1,
                id: "5e388495c7fbaf045cd0cfad"
            }]
        }
        this.props.nested.checkOut(payload, this.props.stripe)


    }
    render() {
        const { buttonText, buttonClass, password } = this.props
        return (


            <MDBBtn
                disabled={password.length < 6}
                onClick={this.handleSubmit}
                className={buttonClass}
                type="button"
            >{buttonText || "Missing Text"} </MDBBtn>

        )
    }
}
const CheckOutButton = injectStripe(_CheckOutButton)

class Landing extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            collapseID: "",
            form: JSON.parse(JSON.stringify(formRaw)),
            purchaseEmail: "",
            purchaseEmail2: "",
            showPassword: false,
            showPassword2: false,
            password: "",
            password2: "",
            error: "",
            success: ""
        }
        this.subscribe = this.subscribe.bind(this)

    }
    toggleCollapse = (collapseID) => {
        this.setState(prevState => ({
            ...this.state,
            collapseID: prevState.collapseID !== collapseID ? collapseID : ""
        }));
    }
    handleChange = (e) => {
        const { value, name } = e.target
        let { form } = this.state
        form[name].value = value
        this.setState({
            ...this.state,
            form
        })
    }
    handleChangeGlobal = (e) => {
        const { value, name } = e.target
        this.setState({
            ...this.state,
            [name]: value
        })
    }
    subscribe = () => {
        const formData = new FormData();
        debugger;
        if (!this.state.form["email"].value) {
            this.setState({
                ...this.state,
                error: "missing email",
            })
            return
        }

        formData.append(`entry.${this.state.form.email.id}`, this.state.form.email.value)
        formData.append(`entry.${this.state.form.payed.id}`, this.state.form.payed.value)

        axios({
            url: `${config.cors}${config.formUrl}`,
            method: `post`,
            data: formData,
            responseType: `json`
        })
            .then((response) => {
                this.setState({
                    ...this.state,
                    success: "Email registered!",
                    error: "",
                    form: JSON.parse(JSON.stringify(formRaw))
                })
            })
            .catch(err => {
                console.log(err);
                this.setState({
                    ...this.state,
                    error: "An error occurred. Please Try again",
                    form: JSON.parse(JSON.stringify(formRaw))
                })
            })
    }
    render() {
        const { collapseID, purchaseEmail, purchaseEmail2, password, password2, showPassword, showPassword2 } = this.state;
        return (

            <>
                <MDBContainer className="landing-main-container">
                    <MDBRow id="section-1">
                        <MDBCol md="6">
                            <span className="course-headline-1">Applied Machine Learning for data Science</span>
                            <span className="course-description-1">
                                Finally, you have found the course you were searching for! <br />
                                Start your career as Data Scientist with the <span className="description-highlight">Applied Machine Learning for data Science</span> online course.</span>
                            <span className="course-description-2">
                                This course is designed for those who desire to become Data Scientist because they are passionate about it, but they don't really know how to get started.
                            </span>
                            <Countdown endDate={"2020-02-07T00:00:00.000Z"} />
                        </MDBCol>
                        <MDBCol md="6">
                            <Image1 className="landing-image-1 z-depth-2" />
                            <Image2 className="landing-image-2  z-depth-2" />
                            <Image3 className="landing-image-3  z-depth-2" />
                        </MDBCol>
                    </MDBRow>
                </MDBContainer>
                <MDBContainer>
                    <MDBRow id="section-2">
                        <MDBCol md="6">
                            <span className="description-1">Join the classroom and get access to <span className="description-highlight">professional tools and techniques</span></span>
                        </MDBCol>
                        <MDBCol md="6">
                            <span className="description-2">You are about to access the most advanced and complete training to become <b>successful Data Scientist</b> in the new data science world and to accelerate your career</span>
                        </MDBCol>
                    </MDBRow>
                </MDBContainer>
                <hr className="contrast-divisor-large" />
                <MDBContainer>
                    <MDBRow id="section-3">
                        <MDBCol className="module-sections-wrapper" md="6">
                            <MDBContainer className='md-accordion'>
                                <MDBCard className=''>
                                    {modules.map(mod => {
                                        return (
                                            <React.Fragment key={mod.id}>
                                                <MDBCollapseHeader
                                                    tagClassName='d-flex justify-content-between'
                                                    onClick={() => this.toggleCollapse(mod.id)}
                                                >
                                                    {mod.title}
                                                    <MDBIcon
                                                        icon={mod.id === 'collapse1' ? 'angle-up' : 'angle-down'}
                                                    />
                                                </MDBCollapseHeader>
                                                <MDBCollapse id={mod.id} isOpen={collapseID}>
                                                    <MDBCardBody className="text-justify">
                                                        {mod.text}
                                                    </MDBCardBody>
                                                </MDBCollapse>
                                            </React.Fragment>
                                        )
                                    })}
                                </MDBCard>
                            </MDBContainer>
                        </MDBCol>
                        <MDBCol md="6">
                            <span className="description-1 mb-4">Unlock all modules of the Olympus of <span className="description-highlight">Data Science Training</span></span>
                            <Image4 className="landing-image-4  z-depth-2" />
                            <Image5 className="landing-image-5  z-depth-2" />
                            <Image6 className="landing-image-6  z-depth-2" />
                        </MDBCol>
                    </MDBRow>
                </MDBContainer>
                <MDBContainer id="section-refund">
                    <MDBRow>
                        <MDBCol className="mt-4" md="6">
                            <span className="title">30-day Refund Guarantee</span>
                            <span className="description-1">Get a full refund at any time within 30 days of enrolling. No reason required.</span>
                            <MDBRow>
                                {showPassword2 ?
                                    <>
                                        <MDBCol size="8" className="offset-2 offset-md-0" md="6">
                                            <MDBInput className="input-password-1" onChange={this.handleChangeGlobal} name="password2" value={password2} hint="Your password" type="password" />
                                        </MDBCol>
                                        <MDBCol md="6" className="d-flex align-items-center justify-content-center">

                                            <StripeProvider apiKey={stripe.public_key}>
                                                <Elements>
                                                    <CheckOutButton nested={this.props} email={purchaseEmail2} password={password2} buttonClass={"cta"} buttonText={"Go to Checkout"} />
                                                </Elements>
                                            </StripeProvider>
                                        </MDBCol>
                                    </>
                                    :
                                    <>
                                        <MDBCol size="8" className="offset-2 offset-md-0" md="6">
                                            <MDBInput className="input-email-1" onChange={this.handleChangeGlobal} name="purchaseEmail2" value={purchaseEmail2} hint="Your e-mail" type="email" />
                                        </MDBCol>
                                        <MDBCol md="6" className="d-flex align-items-center justify-content-center">

                                            <MDBBtn
                                                disabled={!purchaseEmail2}
                                                onClick={() => this.setState({ ...this.state, showPassword2: true })}
                                                className="btn-show-password"
                                                type="button"
                                            >{"Make me a Data Scientist"} </MDBBtn>
                                        </MDBCol>
                                    </>
                                }

                                <span className="therms-disclaimer"> (by proceeding you agree to the <Link className="contrast-text" to="/">terms and privacy policy </Link> )</span>

                            </MDBRow>

                        </MDBCol>
                        <MDBCol className="mt-4" md="6">
                            <Refund className="refund z-depth-1" />
                        </MDBCol>
                    </MDBRow>

                </MDBContainer>
                <div className="z-depth-1 enrollment-options-wrapper">
                    <MDBRow className="m-0 justify-content-center align-items-center d-flex">
                        {
                            /**
                             * 
                             *   <Blob1 className="svg-overlap" id="blob-1" />
                        <Blob2 className="svg-overlap" id="blob-2" />
                        <Blob3 className="svg-overlap" id="blob-3" />
                        <Blob4 className="svg-overlap" id="blob-4" />
                        <Blob5 className="svg-overlap" id="blob-5" />
                        <Blob5 className="svg-overlap" id="blob-6" />
                          <MDBCol md="6">
                            <span className="enrollment-cta-title">Are you against committing to <span className="enrollment-cta-title-highlight">buy this course now?</span></span>
                        </MDBCol>
                             * 
                             */
                        }
                        <Enroll1 className="svg-overlap" id="en-1" />
                        <Enroll2 className="svg-overlap" id="en-2" />
                        <Enroll3 className="svg-overlap" id="en-3" />
                        <Enroll4 className="svg-overlap" id="en-4" />

                        <MDBCol md="6">
                            <div className="enrollement-card z-depth-2">
                                <span className="enrollment-title">Applied Machine Learning for Data Science</span>
                                <hr className="enrollment-divisor-small" />

                                <div className="new-price-wrapper">
                                    <span className="title">Launch Price</span>
                                    <span className="old-price">$597 USD <div className="linee-del"></div></span>
                                    <span className="new-price">$297 USD</span>
                                </div>
                                <hr className="enrollment-divisor-large" />
                                <ul className="enrollment-option-list">
                                    <li>
                                        <span className="enrollment-option">Get Unlimited access to all modules</span>
                                    </li>
                                    <li>
                                        <span className="enrollment-option">3 Portfolio-Ready Capstone</span>
                                    </li>
                                    <li>
                                        <span className="enrollment-option">30-Day Refund Guarantee</span>
                                    </li>
                                    <li>
                                        <span className="enrollment-option">Lifetime Access & Updates</span>
                                    </li>
                                    <li>
                                        <span className="enrollment-option highlight-option">Are you against committing to buy this course now?</span>
                                    </li>
                                </ul>
                                <MDBRow>
                                    <MDBCol className="offset-md-3 offset-2" size="8" md="6">
                                        <MDBInput className="m-0 input-email-1" value={purchaseEmail} name="purchaseEmail" onChange={this.handleChangeGlobal} hint="Your e-mail" type="email" />
                                    </MDBCol>
                                </MDBRow>
                                <StripeProvider apiKey={stripe.public_key}>
                                    <Elements>
                                        <CheckOutButton nested={this.props} email={purchaseEmail} buttonClass={"enrollment-cta"} buttonText={"Make me a Data Scientist"} />
                                    </Elements>
                                </StripeProvider>

                                <span className="therms-disclaimer"> (by proceeding you agree to the <Link className="contrast-text" to="/">terms and privacy policy </Link> )</span>

                            </div>
                        </MDBCol>

                    </MDBRow>
                </div>
                <MDBContainer id="section-2021">
                    <MDBRow>
                        <MDBCol className="mt-4" md="6">
                            <NextYear className="refund z-depth-1" />
                        </MDBCol>
                        <MDBCol className="mt-4" md="6">
                            <span className="title">Save me a reminder</span>
                            <span className="description-1">If you only buy <span className="contrast-text">one course each year</span>, please remember to buy this in genuary 2021*. </span>
                            <span className="description-2">*Price will rise </span>
                            <MDBRow>
                                <MDBCol size="8" className="offset-2 offset-md-0" md="6">
                                    <MDBInput onChange={this.handleChange} name="email" value={this.state.form.email.value} className="input-email-1" hint="Your e-mail" type="email" />
                                    {this.state.error &&
                                        <span className="subscribe-error">{this.state.error}</span>

                                    }
                                    {this.state.success &&
                                        <span className="subscribe-success">{this.state.success}</span>

                                    }

                                </MDBCol>
                                <MDBCol md="6" className="d-flex align-items-center justify-content-center">
                                    <MDBBtn onClick={() => this.subscribe()} className="cta">I'll delay my dreams</MDBBtn>

                                </MDBCol>
                                <span className="therms-disclaimer"> (by proceeding you agree to the terms and privacy policy )</span>

                            </MDBRow>

                        </MDBCol>

                    </MDBRow>

                </MDBContainer>
                <MDBContainer className='md-accordion mt-5 mb-5'>
                    <span className="section-title">Your <span className="description-highlight">most asked</span> questions</span>
                    <MDBCard className='mt-3 mb-5'>
                        {qa.map(question => {
                            return (
                                <React.Fragment key={question.id}>
                                    <MDBCollapseHeader
                                        tagClassName='d-flex justify-content-between'
                                        onClick={() => this.toggleCollapse(question.id)}
                                    >
                                        {question.title}
                                        <MDBIcon
                                            icon={question.id === 'collapse1' ? 'angle-up' : 'angle-down'}
                                        />
                                    </MDBCollapseHeader>
                                    <MDBCollapse id={question.id} isOpen={collapseID}>
                                        <MDBCardBody className="text-justify">
                                            {question.text}
                                        </MDBCardBody>
                                    </MDBCollapse>
                                </React.Fragment>
                            )
                        })}

                    </MDBCard>
                </MDBContainer>
            </>
        )
    }
}
const mapStateToProps = (state) => {
    return {}
}
const mapDispatchToProps = dispatch => bindActionCreators({
    checkOut: store.checkOut
}, dispatch)
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Landing);