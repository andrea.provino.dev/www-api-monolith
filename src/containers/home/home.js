import React from 'react'
import "../home/home"
import ListProduct from '../../components/products/product.all'
import { MDBContainer } from 'mdbreact'

class Home extends React.Component {
    render() {
        return (
            <MDBContainer>
                <ListProduct />
            </MDBContainer>

        )
    }
}
export default Home