import React from "react"
import { MDBContainer, MDBRow, MDBCol, MDBIcon, MDBBtn, MDBInput } from "mdbreact"
import axios from 'axios'

// css
import "./index.css"


/** 💰 Store 💰 **/
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import courseFx from "../../redux/course/course.store"
import moduleFx from "../../redux/module/module.store"
import { getCourses, getCoursesError, getCoursesStatus } from '../../redux/course/course.reducer'
import { getLoginUser } from '../../redux/login/login.reducer'
const config = {
    cors: "https://cors-anywhere.herokuapp.com/",
    formUrl: "https://docs.google.com/forms/d/e/1FAIpQLSfddyxuCkex_Zs7IvLkXqWRlLh75QXJsMMH-rqIDACMP_fRCw/formResponse"
}
const formRaw = {
    email: {
        id: 573871407,
        value: ""
    },
    message: {
        id: 1006546796,
        value: ""
    },
    course: {
        id: 1684132014,
        value: ""
    },
    module: {
        id: 666041650,
        value: ""
    },
    section: {
        id: 1193754452,
        value: ""
    }
}
class Section extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            form: JSON.parse(JSON.stringify(formRaw)),
            error: "",
            success: ""
        }
    }
    handleChange = (e) => {
        e.preventDefault();
        const { name, value } = e.target
        let { form } = this.state
        form[name].value = value
        this.setState({
            ...this.state,
            form
        })
    }
    sendMessage = () => {
        const formData = new FormData();


        formData.append(`entry.${this.state.form.email.id}`, this.props.user.email)
        formData.append(`entry.${this.state.form.course.id}`, this.state.form.course.value)
        formData.append(`entry.${this.state.form.message.id}`, this.state.form.message.value)
        formData.append(`entry.${this.state.form.module.id}`, this.state.form.module.value)
        formData.append(`entry.${this.state.form.section.id}`, this.state.form.section.value)

        axios({
            url: `${config.cors}${config.formUrl}`,
            method: `post`,
            data: formData,
            responseType: `json`
        })
            .then((response) => {
                this.setState({
                    ...this.state,
                    success: "Message Sent",
                    error: "",
                    form: JSON.parse(JSON.stringify(formRaw))
                })
            })
            .catch(err => {
                console.log(err);
                this.setState({
                    ...this.state,
                    error: "An error occurred. Please Try again",
                    form: JSON.parse(JSON.stringify(formRaw))
                })
            })
    }
    componentDidMount = () => {
        this.props.getCourses()
    }
    render() {
        const { courses } = this.props
        return (
            <>
                <MDBContainer>
                    <MDBRow>
                        <MDBCol>
                            <div className="embed-responsive embed-responsive-16by9">
                                <iframe className="embed-responsive-item" src="https://www.youtube.com/embed/vlDzYIIOYmM" allowfullscreen></iframe>
                            </div>
                        </MDBCol>
                    </MDBRow>
                    <MDBRow>
                        <MDBCol className="theory-wrapper" md="6">
                            <div className="theory-card z-depth-1">
                                <div className="justify-content-center d-flex">
                                    <MDBIcon size="3x" icon="school" />
                                </div>

                                <span className="theory-card-title">Theory</span>
                                <div className="justify-content-center d-flex">
                                    <MDBBtn className="theory-button">Theory Book</MDBBtn>
                                </div>
                            </div>
                        </MDBCol>
                        <MDBCol className="practice-wrapper" md="6">
                            <div className="practice-card z-depth-1">
                                <div className="justify-content-center d-flex">
                                    <MDBIcon size="3x" icon="rocket" />
                                </div>

                                <span className="practice-card-title">Practice</span>
                                <div className="justify-content-center d-flex">
                                    <MDBBtn className="practice-button">Practice Book</MDBBtn>
                                </div>

                            </div>
                        </MDBCol>
                    </MDBRow>
                    <MDBRow>
                        <MDBCol>
                            <div className="help-card z-depth-1">
                                <MDBRow className="m-0 p-0">
                                    <MDBCol className="m-0 p-0 d-flex align-items-center justify-content-center" md="4">
                                        <div className="text-center">
                                            <MDBIcon size="3x" icon="concierge-bell" />
                                            <span className="help-card-title">Help</span>
                                        </div>


                                    </MDBCol>
                                    <MDBCol className="textarea-wrapper m-0 p-0" md="8">
                                        <MDBInput onChange={this.handleChange} name="message" type="textarea" label="Let's solve the problem together!" rows="1" />
                                        {this.state.success && <span>{this.state.success}</span>}
                                        <div className="float-right">
                                            <MDBBtn onClick={this.sendMessage} disabled={this.state.form.message.value.length < 15} className="practice-button">Help me</MDBBtn>
                                        </div>
                                    </MDBCol>
                                </MDBRow>
                            </div>
                        </MDBCol>
                    </MDBRow>
                </MDBContainer>

            </>
        )
    }
}

const mapStateToProps = (state /**, ownProps */) => {
    return {
        error: getCoursesError(state.course),
        pending: getCoursesStatus(state.course),
        courses: getCourses(state.course),
        user: getLoginUser(state.auth)
    }
}

const mapDispatchToProps = dispatch => bindActionCreators({
    getCourses: courseFx.getCourses,
    getCourseModules: moduleFx.getModulesByCourseId
}, dispatch)

export default connect(
    mapStateToProps,  //subscribing to the store by mapping store values in our component's props || null
    mapDispatchToProps //inject action creators in our component's props
)(Section)