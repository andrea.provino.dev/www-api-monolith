import React from "react"
import { MDBContainer, MDBRow, MDBCol } from "mdbreact"
// css
import "./index.css"


/** 💰 Store 💰 **/
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import courseFx from "../../redux/course/course.store"
import moduleFx from "../../redux/module/module.store"
import { getCourses, getCoursesError, getCoursesStatus } from '../../redux/course/course.reducer'


class Course extends React.Component {
    componentDidMount = () => {
        this.props.getCourses()
    }
    render() {
        const { courses } = this.props
        return (
            <>
                <MDBContainer>
                    {courses.map(course => {
                        return (
                            <MDBRow key={course._id} className="mt-2 mb-5">
                                <MDBCol>
                                    <div onClick={() => this.props.getCourseModules(course._id)} className="course-card">
                                        <MDBRow>
                                            <MDBCol className="image-wrapper" md="4">
                                                <img src={course.pictures[0]} className="image img-fluid" alt="CourseImage" />
                                            </MDBCol>
                                            <MDBCol className="body-wrapper" md="8">
                                                <div className="body z-depth-1">
                                                    <span className="body-title">{course.name}</span>
                                                    <span className="body-description">{course.description}</span>
                                                    <span className="body-description"></span>
                                                </div>

                                            </MDBCol>
                                        </MDBRow>
                                    </div>
                                </MDBCol>
                            </MDBRow>
                        )

                    })}
                </MDBContainer>

            </>
        )
    }
}

const mapStateToProps = (state /**, ownProps */) => {
    return {
        error: getCoursesError(state.course),
        pending: getCoursesStatus(state.course),
        courses: getCourses(state.course)
    }
}

const mapDispatchToProps = dispatch => bindActionCreators({
    getCourses: courseFx.getCourses,
    getCourseModules: moduleFx.getModulesByCourseId
}, dispatch)

export default connect(
    mapStateToProps,  //subscribing to the store by mapping store values in our component's props || null
    mapDispatchToProps //inject action creators in our component's props
)(Course)