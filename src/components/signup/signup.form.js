/** ✅ Husehold import ✅ **/
import React from 'react'
import { AtomSpinner } from 'react-epic-spinners'
import { MDBAlert, MDBContainer, MDBRow, MDBCol, MDBCard, MDBCardBody, MDBInput, MDBBtn, MDBIcon, MDBModalFooter } from 'mdbreact';
import { Link } from 'react-router-dom'

/** 💰 Store 💰 **/
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';



// import login from store
import "./signup.form.css" //css
import { signUp, verifyEmail } from "../../redux/signup/signup.store"
import { getSingupMessage, getSingupError, getSignUpStatus } from '../../redux/signup/signup.reducer'


class SignupForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            form: {
                email: "andrea.provino.dev+react@gmail.com",
                password: "Example10%",
                verification_code: ""
            },
            info: {
                error: undefined,
                verify_mode_enabled: false
            }
        }
    }
    handleChange = (e) => {
        const field = e.target.name;
        const value = e.target.value;
        let old_form = this.state.form;
        old_form[field] = value
        const new_form = old_form
        this.setState({
            ...this.state,
            form: new_form
        })
    }

    checkErrors = (form) => {
        let missing = [];
        Object.keys(form).forEach(field => {
            if (field !== "verification_code" && !this.props.message) {
                if (!form[field]) missing.push(field)
            } else if (field === "verification_code" && (this.props.message || this.state.info.verify_mode_enabled)) {
                if (!form[field]) missing.push(field)
            }

        })
        return missing
    }
    enableVerifyMode = () => {
        this.setState({
            ...this.state,
            info: {
                ...this.state.info,
                verify_mode_enabled: true
            }
        })
    }
    handleSubmit = async (e) => {
        // simply verify email
        if (this.state.info.verify_mode_enabled || this.props.message) {
            const form = this.state.form
            const field_missing = this.checkErrors(form)
            if (!field_missing.length) {
                let payload = {
                    "verificationCode": form.verification_code,
                    "username": form.email
                }
                this.props.verifyEmail(payload)
        
            } else {
                this.setState({
                    ...this.state,
                    info: {
                        ...this.state.info,
                        error: "Please check: " + field_missing.join(", ")
                    }
                })
            }
        } else {
            const form = this.state.form
            const field_missing = this.checkErrors(form)
            if (!field_missing.length) {
                let payload = {
                    "password": form.password,
                    "username": form.email
                }
                this.props.signUp(payload)
            } else {
                this.setState({
                    ...this.state,
                    info: {
                        ...this.state.info,
                        error: "Please check: " + field_missing.join(", ")
                    }
                })
            }
        }


    }
    render() {
        const { message, error, pending } = this.props;
        const { info } = this.state
        return (
            <MDBContainer>
                {pending && <AtomSpinner color="#5ed1fc" />}
                <MDBRow className="d-flex justify-content-center" >
                    <MDBCol md="6">
                        <MDBCard>
                            <MDBCardBody className="mx-4">
                                <div className="text-center">
                                    <h3 className="dark-grey-text mb-5">
                                        <strong>Sign Up</strong>
                                    </h3>
                                </div>
                                <MDBInput
                                    label="Your email"
                                    type="email"
                                    name="email"
                                    error="Whoops!"
                                    valueDefault="@gmail.com"
                                    onChange={this.handleChange}
                                    value={this.state.form.email}
                                />
                                <MDBInput
                                    label="Your password"
                                    type="password"
                                    name="password"
                                    containerClass="mb-0"
                                    onChange={this.handleChange}
                                    value={this.state.form.password}
                                />
                                {(message || this.state.info.verify_mode_enabled) &&
                                    <React.Fragment>
                                        <MDBAlert color="success">
                                            {message}
                                        </MDBAlert>
                                        <MDBInput
                                            label="Verification Code"
                                            type="number"
                                            name="verification_code"
                                            containerClass="mb-0"
                                            onChange={this.handleChange}
                                            value={this.state.form.verification_code}
                                        />
                                    </React.Fragment>
                                }
                                {info.error && <p>{info.error}</p>}
                                <div className="text-center mb-3">
                                    <MDBBtn
                                        type="button"
                                        gradient="blue"
                                        onClick={this.handleSubmit}
                                        rounded
                                        className="btn-block z-depth-1a">{(message || this.state.info.verify_mode_enabled) ? 'Verify Email' : 'Sign Up'}</MDBBtn>
                                </div>


                            </MDBCardBody>
                            <MDBModalFooter className="mx-5 pt-3 mb-1 d-block">
                                <div className="d-flex justify-content-between">
                                    <p className="font-small grey-text text-right ">Are you member?<Link to="login" className="blue-text ml-1">Sign In</Link></p>
                                    <p className="font-small grey-text text-right ">Just want to verify?<a onClick={this.enableVerifyMode} className="blue-text ml-1">Verify Email</a></p>


                                </div>


                                {error &&
                                    <MDBAlert color="danger">
                                        {error.message} {
                                            error.code === "UsernameExistsException"
                                                ? <Link to="password-lost">Forgot Password</Link>
                                                : ""}
                                    </MDBAlert>
                                }



                            </MDBModalFooter>
                        </MDBCard>
                    </MDBCol>
                </MDBRow>
            </MDBContainer>


        )
    }
}

const mapStateToProps = (state /**, ownProps */) => {
    return {
        error: getSingupError(state.signup),
        pending: getSignUpStatus(state.signup),
        message: getSingupMessage(state.signup)
    }
}

const mapDispatchToProps = dispatch => bindActionCreators({
    signUp: signUp,
    verifyEmail: verifyEmail
}, dispatch)

export default connect(
    mapStateToProps,  //subscribing to the store by mapping store values in our component's props || null
    mapDispatchToProps //inject action creators in our component's props
)(SignupForm)