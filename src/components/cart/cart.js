
/** ✅ Husehold import ✅ **/
import React from "react";
import { MDBRow, MDBCard, MDBCardBody, MDBTooltip, MDBAlert, MDBModalFooter, MDBTable, MDBTableBody, MDBTableHead, MDBBtn, MDBBadge } from "mdbreact";
import { Elements, StripeProvider, injectStripe } from 'react-stripe-elements';

/** 💰 Store 💰 **/
import { bindActionCreators } from "redux"
import { connect } from "react-redux"
import { getCartError, getCartStatus, getCartItems, getCheckoutError, getCheckoutSuccess } from "../../redux/cart/cart.reducer"
import { getLoginUser } from "../../redux/login/login.reducer"
import store from "../../redux/cart/cart.store"
const stripe = {
    private_key: process.env.REACT_APP_PRIVATE_KEY,
    public_key: process.env.REACT_APP_PUBLIC_KEY
}

class _CheckOutButton extends React.Component {
    constructor(props) {
        super(props)

    }
    handleSubmit = async () => {
        const payload = {
            user: this.props.nested.user,
            items: this.props.nested.items
        }
        this.props.nested.checkOut(payload, this.props.stripe)


    }
    render() {
        return (
            <React.Fragment>
                <div className="text-right">
                    <MDBBtn
                        onClick={this.handleSubmit}
                        type="button"
                        outline
                        rounded
                        className="btn z-depth-1a"
                    >Checkout </MDBBtn>
                </div>
            </React.Fragment>
        )
    }
}
const CheckOutButton = injectStripe(_CheckOutButton)

class Cart extends React.Component {
    constructor(props) {
        super(props)
    }
    componentWillMount = () => {
    }
    handleRemoveItem = (item) => {
        this.props.removeItem(item)
    }
    render() {

        const rows = [];
        const columns = [
            {
                label: '',
                field: 'img',
            },
            {
                label: <strong>Name</strong>,
                field: 'name'
            },
            {
                label: <strong>Price</strong>,
                field: 'price'
            },
            {
                label: <strong>QTY</strong>,
                field: 'qty'
            },
            {
                label: <strong>Amount</strong>,
                field: 'amount'
            },
            {
                label: '',
                field: 'button'
            }
        ]
        const { items } = this.props
        items.map(item => {
            return rows.push(
                {
                    'img': <img src={item.pictures[0]} alt="" className="img-fluid z-depth-0" />,
                    'name': <h5 className="mt-3" key={new Date().getDate + 1}><strong>{item.name}</strong></h5>,
                    'price': `€${item.price_regular}`,
                    'qty': <MDBBadge color="success">{item.qty}</MDBBadge>,
                    'amount': <strong>€{(item.qty * item.price_regular).toFixed(2)}</strong>,
                    'button':
                        <MDBTooltip placement="top">
                            <MDBBtn color="danger" onClick={() => this.handleRemoveItem(item)} size="sm">X</MDBBtn>
                            <div>Remove item</div>
                        </MDBTooltip>
                }
            )
        });

        return (
            <StripeProvider apiKey={stripe.public_key}>
                <Elements>
                    <MDBRow className="my-2" center>
                        <MDBCard className="w-100">
                            <MDBCardBody>
                                <MDBTable responsive className="product-table">
                                    <MDBTableHead className="font-weight-bold" color="mdb-color lighten-5" columns={columns} />
                                    <MDBTableBody rows={rows} />
                                </MDBTable>
                            </MDBCardBody>
                            <MDBModalFooter className="pt-4">
                                {this.props.items.length > 0 ?
                                    <CheckOutButton nested={this.props} />
                                    :
                                    <MDBAlert className="w-100" color="warning" >
                                        Add one or more product and see them here before checkout
                            </MDBAlert>
                                }

                            </MDBModalFooter>
                        </MDBCard>
                    </MDBRow >
                </Elements>
            </StripeProvider>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        pending: getCartStatus(state.cart),
        error: getCartError(state.cart),
        items: getCartItems(state.cart),
        user: getLoginUser(state.auth),
        checkoutError: getCheckoutError(state.cart),
        checkoutSuccess: getCheckoutSuccess(state.cart),
    }
}
const mapDispatchToProps = dispatch => bindActionCreators({
    addItem: store.addItem,
    removeItem: store.removeItem,
    removeAll: store.removeAll,
    checkOut: store.checkOut,
}, dispatch)
export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(Cart);