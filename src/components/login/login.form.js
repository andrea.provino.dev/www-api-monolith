/** ✅ Husehold import ✅ **/
import React from 'react'
import { AtomSpinner } from 'react-epic-spinners'
import { MDBContainer, MDBRow, MDBCol, MDBCard, MDBCardBody, MDBInput, MDBBtn, MDBIcon, MDBModalFooter } from 'mdbreact';
import { Link } from 'react-router-dom'

/** 💰 Store 💰 **/
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

/** Router **/
import { history } from "../../router/index"

// import login from store
import "../login/login.form.css" //css
import { login } from "../../redux/login/login.store"
import { getLoginError, getLoginUser, getLoginStatus } from '../../redux/login/login.reducer'


class FormLogin extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            form: {
                email: "andrea.provino.dev+react@gmail.com",
                password: "Legouno1$"
            },
            info: {
                error: undefined
            }
        }
    }
    componentDidMount = () => {
        // do not work
        const { user } = this.props
        if (user) history.push('/classroom')

    }
    handleChange = (e) => {
        const field = e.target.name;
        const value = e.target.value;
        let old_form = this.state.form;
        const new_form = old_form[field] = value
        this.setState({
            ...this.state,
            new_form
        })
    }

    checkErrors = (form) => {
        let missing = [];
        Object.keys(form).forEach(field => {
            if (!form[field]) missing.push(field)
        })
        return missing
    }
    handleSubmit = (e) => {
        const form = this.state.form
        const field_missing = this.checkErrors(form)
        if (!field_missing.length) {
            let payload = {
                "password": form.password,
                "username": form.email
            }
            this.props.login(payload, history);

        } else {
            this.setState({
                ...this.state,
                info: {
                    ...this.state.info,
                    error: "Please check: " + field_missing.join(", ")
                }
            })
        }

    }
    render() {
        const { user, error, pending } = this.props;
        const { info } = this.state
        return (
            <MDBContainer>
                {pending && <AtomSpinner color="#5ed1fc" />}
                <MDBRow className="d-flex justify-content-center" >
                    <MDBCol md="6">
                        <MDBCard>
                            <MDBCardBody className="mx-4">
                                <div className="text-center">
                                    <h3 className="dark-grey-text mb-5">
                                        <strong>Sign in</strong>
                                    </h3>
                                </div>
                                <MDBInput
                                    label="Your email"
                                    type="email"
                                    name="email"
                                    error="Whoops!"
                                    valueDefault="@gmail.com"
                                    onChange={this.handleChange}
                                    value={this.state.form.email}
                                />
                                <MDBInput
                                    label="Your password"
                                    type="password"
                                    name="password"
                                    containerClass="mb-0"
                                    onChange={this.handleChange}
                                    value={this.state.form.password}
                                />
                                {info.error && <p className="error-message">{info.error}</p>}
                                {error && <p className="error-message">{error}</p>}
                                <p className="font-small blue-text d-flex justify-content-end pb-3">
                                    Forgot <a href="#!" className="blue-text ml-1">Password?</a>
                                </p>
                                <div className="text-center mb-3">
                                    <MDBBtn
                                        type="button"
                                        gradient="blue"
                                        onClick={this.handleSubmit}
                                        rounded
                                        className="btn-block z-depth-1a">Sign in </MDBBtn>
                                </div>

                            </MDBCardBody>
                            <MDBModalFooter className="mx-5 pt-3 mb-1">
                                <p className="font-small grey-text d-flex justify-content-end">
                                    Not a member?
                <Link to="signup" className="blue-text ml-1">

                                        Sign Up
                </Link>
                                </p>
                            </MDBModalFooter>
                        </MDBCard>
                    </MDBCol>
                </MDBRow>
            </MDBContainer>


        )
    }
}

const mapStateToProps = (state /**, ownProps */) => {
    return {
        error: getLoginError(state.auth),
        pending: getLoginStatus(state.auth),
        user: getLoginUser(state.auth)
    }
}

const mapDispatchToProps = dispatch => bindActionCreators({
    login: login
}, dispatch)

export default connect(
    mapStateToProps,  //subscribing to the store by mapping store values in our component's props || null
    mapDispatchToProps //inject action creators in our component's props
)(FormLogin)