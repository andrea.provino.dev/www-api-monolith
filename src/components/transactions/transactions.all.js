/** ✅ Husehold import ✅ **/
import React from 'react'
import { MDBInput, MDBContainer, MDBModal, MDBModalBody, MDBModalHeader, MDBModalFooter, MDBAnimation, MDBCard, MDBCardBody, MDBCardHeader, MDBBadge, MDBBtn, MDBDataTable } from 'mdbreact';

/** 💰 Store 💰 **/
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import trx_store from "../../redux/transactions/transaction.store"
import { getTransactions, getTransactionsError, getTransactionsStatus } from "../../redux/transactions/transactions.reducer"
import moment from "moment"
class ListProduct extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            info_item_open: false,
            trx_selected: {
                user_id: {}
            }
        }
    }
    toggle = item => {
        // togle modal opening 
        this.setState({
            ...this.state,
            [item]: !this.state[item]
        })
    }
    componentWillMount = () => {
        // if app product state is empty fetch, otherwise print use cacheù
        !this.props.transactions.length ? this.props.getTransactions() : console.log("Cached products")

    }
    updateList = () => {
        this.props.getTransactions()
    }
    handleRowClick = (row) => {
        this.setState({
            ...this.state,
            trx_selected: row,
            info_item_open: true
        })
    }
    getTrxStatus = (status) => {
        let color = ''
        switch (status) {
            case "PENDING":
                color = "warning"
                break;
            case 'PAYED':
                color = 'success'
                break;
            case 'CANCELLED':
                color = 'danger'
                break;
            case 'REFOUNDED':
                color = 'info'
                break;
            default:
                color = 'default'
        }
        return <MDBBadge color={color}>{status || 'Not available'}</MDBBadge>
    }
    render() {
        const { transactions, pending } = this.props
        const data_panel = {
            columns: [
                {
                    'label': 'Transaction Date',
                    'field': 'created_on',
                    'sort': 'asc',
                    width: 150
                },
                {
                    'label': 'Full Name',
                    'field': 'full_name',
                    'sort': 'asc',
                    width: 150
                },
                {
                    'label': 'Amount',
                    'field': 'amount_payed',
                    'sort': 'asc',
                    width: 120
                },
                {
                    'label': 'Status',
                    'field': 'status',
                    'sort': 'asc',
                    width: 120
                }
            ],
            rows: this.props.transactions.map(item => {
                return {
                    created_on: moment(item.created_on).format("DD/MM/YY HH:mm") || "Not Available",
                    fullname: item.user_id.fullname || (item.user_id.firstname + " " + item.user_id.lastname),
                    amount_payed: (item.amount_payed / 100).toFixed(2).replace(".", ",") + " €",
                    status: this.getTrxStatus(item.status),
                    clickEvent: () => this.handleRowClick(item)
                }
            })
        }
        const { trx_selected } = this.state
        return (
            <React.Fragment>
                <MDBContainer>
                    <MDBModal isOpen={this.state.info_item_open} toggle={() => this.toggle("info_item_open")} fullHeight position="right">
                        <MDBModalHeader toggle={() => this.toggle("info_item_open")}>Transaction Detail</MDBModalHeader>
                        <MDBModalBody>
                            <MDBInput label="Customer" disabled value={trx_selected.user_id.fullname || (trx_selected.user_id.firstname + " " + trx_selected.user_id.lastname)} />
                            <MDBInput label="Customer Email" disabled value={trx_selected.user_id.email} />
                            <MDBInput label="Customer Phone Number" disabled value={trx_selected.user_id.phone_number} />
                            <MDBInput label="Amount" disabled value={(trx_selected.amount / 100).toFixed(2).replace(".", ",") + " €"} />
                            <MDBInput label="Amount Payed" disabled value={(trx_selected.amount_payed / 100).toFixed(2).replace(".", ",") + " €"} />
                            <MDBInput label="Created On" disabled value={trx_selected.created_on || "Not Available"} />
                            <MDBInput label="Payed On" disabled value={trx_selected.payed_on || "Not Available"} />
                            <MDBInput label="Stripe Session ID" disabled value={trx_selected.stripe_session_id} />
                            <MDBInput label="Status" disabled value={trx_selected.status} />
                        </MDBModalBody>
                        <MDBModalFooter>
                            <MDBBtn color="info" onClick={() => this.toggle("info_item_open")}>Close</MDBBtn>
                            <MDBBtn disabled color="success">Save changes</MDBBtn>
                        </MDBModalFooter>
                    </MDBModal>
                </MDBContainer>
                <MDBCard narrow>
                    <MDBCardHeader className="view view-cascade gradient-card-header blue-gradient d-flex justify-content-between align-items-center py-2 mx-4 mb-3">
                        <div>
                            <MDBBtn outline rounded size="sm" color="white" className="px-2">
                                <i className="fa fa-th-large mt-0"></i>
                            </MDBBtn>
                            <MDBBtn outline rounded size="sm" color="white" className="px-2">
                                <i className="fa fa-columns mt-0"></i>
                            </MDBBtn>
                        </div>
                        <a href="#" className="white-text mx-3">Transactions</a>
                        <div>
                            <MDBBtn disabled onClick={() => this.props.history.push("")} outline rounded size="sm" color="white" className="px-2">
                                <i className="fas fa-plus mt-0"></i>
                            </MDBBtn>
                            <MDBBtn onClick={this.updateList} outline rounded size="sm" color="white" className="px-2">
                                {pending ?
                                    <MDBAnimation infinite type="flash">
                                        <i className="fa fa-arrow-down mt-0"></i>
                                    </MDBAnimation>
                                    :
                                    <i className="fa fa-sync mt-0"></i>
                                }
                            </MDBBtn>


                        </div>
                    </MDBCardHeader>
                    <MDBCardBody cascade>
                        <MDBDataTable className="custom-data-table" data={data_panel} scrollX>
                        </MDBDataTable>
                    </MDBCardBody>
                </MDBCard >

            </React.Fragment>
        )
    }
}

const mapStateToProps = (state /**, ownProps */) => {
    return {
        error: getTransactionsError(state.transactions),
        pending: getTransactionsStatus(state.transactions),
        transactions: getTransactions(state.transactions)
    }
}

const mapDispatchToProps = dispatch => bindActionCreators({
    getTransactions: trx_store.getTransactions
}, dispatch)

export default connect(
    mapStateToProps,  //subscribing to the store by mapping store values in our component's props || null
    mapDispatchToProps //inject action creators in our component's props
)(ListProduct)
