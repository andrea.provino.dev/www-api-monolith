/** ✅ Husehold import ✅ **/
import React, { Component } from "react";
import {
    MDBIcon,
    MDBNavbar, MDBNavbarNav, MDBNavItem, MDBNavLink, MDBNavbarToggler
} from "mdbreact";

/** 💰 Store 💰 **/
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';


// import login from store
import { logout } from "../../redux/login/login.store"
import { getLoginError, getLoginUser, getLoginStatus } from '../../redux/login/login.reducer'

//import csss
import "./navbar.css"
class NavbarPage extends Component {
    constructor(props) {
        super(props)
    }
    state = {
        isOpen: false
    };

    handleToggle = () => {
        this.props.onSideNavToggleClick()
    }

    render() {
        const { user } = this.props
        return (
            <MDBNavbar scrolling
                fixed='top' expand='md' className="flexible-MDBNavbar custom-navbar-dashboard">
                <MDBIcon onClick={this.handleToggle} icon='space-shuttle' />
                <MDBNavbarNav right>
                    <MDBNavItem>
                        <MDBNavLink to='/'>
                            <MDBIcon icon='igloo' />
                            <span className='d-none d-md-inline ml-1'>Home</span>
                        </MDBNavLink>
                    </MDBNavItem>
                </MDBNavbarNav>

            </MDBNavbar>

        );
    }
}
const mapStateToProps = (state /**, ownProps */) => {
    return {
        error: getLoginError(state.auth),
        pending: getLoginStatus(state.auth),
        user: getLoginUser(state.auth)
    }
}

const mapDispatchToProps = dispatch => bindActionCreators({
    logout: logout
}, dispatch)

export default connect(
    mapStateToProps,  //subscribing to the store by mapping store values in our component's props || null
    mapDispatchToProps //inject action creators in our component's props
)(NavbarPage)