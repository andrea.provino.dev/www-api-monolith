/** ✅ Husehold import ✅ **/
import React, { Component } from "react";
import {
    MDBNavbar, MDBNavbarBrand, MDBNavbarNav, MDBNavItem, MDBNavLink, MDBNavbarToggler, MDBCollapse
} from "mdbreact";

/** 💰 Store 💰 **/
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { logout } from "../../redux/login/login.store"
import { getLoginError, getLoginUser, getLoginStatus } from '../../redux/login/login.reducer'
import store_cart from "../../redux/cart/cart.store"
import { getCartError, getCartStatus, getCartItems } from "../../redux/cart/cart.reducer"

//import csss
import "./navbar.css"
class NavbarPage extends Component {
    state = {
        isOpen: false
    };

    toggleCollapse = () => {
        this.setState({ isOpen: !this.state.isOpen });
    }

    render() {
        const { user } = this.props.login
        const { items } = this.props.cart
        return (
            <MDBNavbar scrolling double fixed className="custom-navbar-ecommerce" dark expand="md">
                <MDBNavbarBrand>
                    <strong className="white-text">E-Commerce 1.0</strong>
                </MDBNavbarBrand>
                <MDBNavbarToggler onClick={this.toggleCollapse} />
                <MDBCollapse id="navbarCollapse3" isOpen={this.state.isOpen} navbar>
                    <MDBNavbarNav right>
                        <MDBNavItem>
                            <MDBNavLink to="/">Home</MDBNavLink>
                        </MDBNavItem>
                        <MDBNavItem>
                            <MDBNavLink to="/dashboard/users">Dashboard</MDBNavLink>
                        </MDBNavItem>
                        {user ?
                            <MDBNavItem>
                                <MDBNavLink to="#" onClick={this.props.logout}>Logout</MDBNavLink>
                            </MDBNavItem>
                            :

                            <MDBNavItem>
                                <MDBNavLink to="/login">Login</MDBNavLink>
                            </MDBNavItem>
                        }
                        <MDBNavItem>
                            <MDBNavLink to="/cart">Cart [{items.length}]</MDBNavLink>
                        </MDBNavItem>
                    </MDBNavbarNav>

                </MDBCollapse>
            </MDBNavbar>

        );
    }
}
const mapStateToProps = (state /**, ownProps */) => {
    return {
        login: {
            error: getLoginError(state.auth),
            pending: getLoginStatus(state.auth),
            user: getLoginUser(state.auth)
        },
        cart: {
            pending: getCartStatus(state.cart),
            error: getCartError(state.cart),
            items: getCartItems(state.cart),
        }

    }
}

const mapDispatchToProps = dispatch => bindActionCreators({
    logout: logout
}, dispatch)

export default connect(
    mapStateToProps,  //subscribing to the store by mapping store values in our component's props || null
    mapDispatchToProps //inject action creators in our component's props
)(NavbarPage)