/** ✅ Husehold import ✅ **/
import React, { Component } from "react";
import { MDBSideNavCat, MDBSideNavNav, MDBSideNav, MDBSideNavLink, MDBContainer, MDBIcon } from "mdbreact";
import './sidebar.css'

class SidebarADmin extends Component {
    constructor(props) {
        super(props)
    }
    state = {
        sideNavLeft: true,
        breakWidth: 1200,
        windowWidth: 0,
        sideNavToggled: false,
    }

    sidenavToggle = sidenavId => () => {
        const sidenavNr = `sideNav${sidenavId}`
        this.setState({
            [sidenavNr]: !this.state[sidenavNr]
        });
    };
    componentDidMount() {
        this.handleResize();
        window.addEventListener('resize', this.handleResize);
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.handleResize);
    }
    handleResize = () => {
        this.setState({
            windowWidth: window.innerWidth
        });
    };
    toggleSideNav = () => {
        if (this.state.windowWidth < this.state.breakWidth) {
            this.setState({
                sideNavToggled: !this.state.sideNavToggled
            });
        }
    };
    rSNL = (to, text, icon, css) => {
        return (
            <MDBSideNavLink className={css} to={to}>
                <MDBIcon icon={icon} className="mr-2" />{text}
            </MDBSideNavLink>
        )
    }
    render() {
        return (
            <MDBContainer>
                <MDBSideNav breakWidth={this.state.breakWidth} fixed triggerOpening={this.props.triggerOpening}

                    className="custom-sidebar-admin">
                    <li>
                        <div className="logo-wrapper sn-ad-avatar-wrapper">
                            <a href="#!">
                                <img alt="" src="https://mdbootstrap.com/img/Photos/Avatars/img%20(10).jpg" className="rounded-circle" />
                                <span style={{ color: "#00a8b6" }}>Admin</span>
                            </a>
                        </div>
                    </li>

                    <MDBSideNavNav>
                        {this.rSNL('/dashboard/users', 'Users', 'user-astronaut', 'sidebar-link-SO')}
                        <MDBSideNavCat className="sidebar-link-MO" name="Products" id="prod" icon="chevron-right">
                            {this.rSNL('/dashboard/product/all', 'All', 'cubes', 'sidebar-link-MO-O')}
                            {this.rSNL('/dashboard/product/add', 'Add', 'cube', 'sidebar-link-MO-O')}
                        </MDBSideNavCat>
                        <MDBSideNavCat className="sidebar-link-MO" name="Transactions" id="trx" icon="chevron-right">
                            {this.rSNL('/dashboard/transactions/all', 'All', 'cash-register', 'sidebar-link-MO-O')}
                        </MDBSideNavCat>
                    </MDBSideNavNav>
                </MDBSideNav>
            </MDBContainer >

        );
    }
}

export default SidebarADmin;


