/** ✅ Husehold import ✅ **/
import React, { Component } from "react";
import { MDBSideNavCat, MDBSideNavNav, MDBSideNav, MDBSideNavLink, MDBContainer, MDBIcon } from "mdbreact";
import './sidebar.css'


/** 💰 Store 💰 **/
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import store from "../../redux/module/module.store"
import { getModules, getModulesError, getModulesStatus } from '../../redux/module/module.reducer'


class SidebarADmin extends Component {
    constructor(props) {
        super(props)
    }
    state = {
        sideNavLeft: true,
        breakWidth: 1200,
        windowWidth: 0,
        sideNavToggled: false,
    }

    sidenavToggle = sidenavId => () => {
        const sidenavNr = `sideNav${sidenavId}`
        this.setState({
            [sidenavNr]: !this.state[sidenavNr]
        });
    };
    componentDidMount() {
        this.handleResize();
        window.addEventListener('resize', this.handleResize);
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.handleResize);
    }
    handleResize = () => {
        this.setState({
            windowWidth: window.innerWidth
        });
    };
    toggleSideNav = () => {
        if (this.state.windowWidth < this.state.breakWidth) {
            this.setState({
                sideNavToggled: !this.state.sideNavToggled
            });
        }
    };
    rSNL = (to, text, icon, css) => {
        return (
            <MDBSideNavLink className={css} to={to}>
                <MDBIcon icon={icon} className="mr-2" />{text}
            </MDBSideNavLink>
        )
    }
    render() {
        const { modules } = this.props
        return (
            <MDBContainer>
                <MDBSideNav breakWidth={this.state.breakWidth} fixed triggerOpening={this.props.triggerOpening}

                    className="custom-sidebar-admin">
                    <li>
                        <div className="logo-wrapper sn-ad-avatar-wrapper">
                            <a href="#!">
                                <img alt="" src="https://mdbootstrap.com/img/Photos/Avatars/img%20(10).jpg" className="rounded-circle" />
                                <span style={{ color: "#00a8b6" }}>User</span>
                            </a>
                        </div>
                    </li>

                    <MDBSideNavNav>
                        {this.rSNL('/dashboard/users', 'Me', 'user-astronaut', 'sidebar-link-SO')}
                        {modules.map(mod => {
                            return (
                                <MDBSideNavCat className={"sidebar-link-MO " + (mod.is_active ? "" : "disabled")} name={mod.name} id={mod._id} icon="chevron-right">
                                    {this.rSNL('/dashboard/transactions/all', 'All', 'cash-register', 'sidebar-link-MO-O')}
                                    {this.rSNL('/dashboard/transactions/all', 'All', 'cash-register', 'sidebar-link-MO-O')}
                                </MDBSideNavCat>
                            )
                        })}
                    </MDBSideNavNav>
                </MDBSideNav>
            </MDBContainer >

        );
    }
}


const mapStateToProps = (state /**, ownProps */) => {
    return {
        error: getModulesError(state.module),
        pending: getModulesStatus(state.module),
        modules: getModules(state.module)
    }
}

const mapDispatchToProps = dispatch => bindActionCreators({

}, dispatch)

export default connect(
    mapStateToProps,  //subscribing to the store by mapping store values in our component's props || null
    mapDispatchToProps //inject action creators in our component's props
)(SidebarADmin)




