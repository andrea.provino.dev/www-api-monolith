/** ✅ Husehold import ✅ **/
import React from 'react'
import { AtomSpinner } from 'react-epic-spinners'
// import { MDBCard, MDBCardImage, MDBIcon, MDBRow, MDBCol, } from 'mdbreact';
import { MDBRow, MDBCol, MDBCard, MDBCardBody, MDBCardImage, MDBCardTitle, MDBCardText, MDBTooltip, MDBCardFooter, MDBIcon, MDBBtn } from
    "mdbreact";

/** 💰 Store 💰 **/
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import store_product from "../../redux/products/products.store"
import { getProducts, getProductsError, getProductsStatus } from '../../redux/products/products.reducer'
import store_cart from "../../redux/cart/cart.store"

class ListProduct extends React.Component {
    constructor(props) {
        super(props)
        this.state = {

        }
        this.handleItemSelected = this.handleItemSelected.bind(this)
    }
    componentWillMount = () => {
        this.props.getProducts()
    }
    handleItemSelected = (product) => {
        this.props.addItem(product)
    }
    render() {
        const { products, pending } = this.props
        //const callBack_image = "https://mdbootstrap.com/img/Photos/Horizontal/E-commerce/Vertical/img%20(5).jpg"
        const callBack_image = "https://mdbootstrap.com/img/Photos/Horizontal/E-commerce/Products/shoes%20(3).jpg"
        return (
            <MDBRow className="m-auto d-flex justify-content-center">
                {pending && <AtomSpinner color="#5ed1fc" />}
                {
                    products.map(product => {
                        const picture = !product.pictures.length ? callBack_image : product.pictures[0]
                        return (
                            /** 
                            <MDBCard key={product._id} className="mx-4 my-4" style={{ width: "18rem", minHeight: "29.12rem" }} collection>
                                <MDBCardImage className="img-fluid" zoom src={picture} />
                                <div className="stripe dark">
                                    <a href="#!">
                                        <p>{product.name}
                                            <MDBIcon icon="chevron-right" />
                                        </p>
                                    </a>
                                </div>
                            </MDBCard>
                            */
                            <MDBCard key={product._id} className="m-2" style={{ width: "22rem" }} cascade ecommerce wide>
                                <MDBCardImage cascade top src={picture}
                                    waves />
                                <MDBCardBody cascade className="text-center">
                                    <MDBCardTitle>
                                        <a href="#!"><strong>{product.name}</strong></a>
                                    </MDBCardTitle>
                                    <ul className="rating">
                                        <li>
                                            <MDBIcon icon="star" />
                                        </li>
                                        <li>
                                            <MDBIcon icon="star" />
                                        </li>
                                        <li>
                                            <MDBIcon icon="star" />
                                        </li>
                                        <li>
                                            <MDBIcon icon="star" />
                                        </li>
                                        <li>
                                            <MDBIcon className="far" icon="star" />
                                        </li>
                                    </ul>
                                    <MDBCardText>
                                        {product.description}
                                    </MDBCardText>
                                    <MDBCardFooter>
                                        <span className="float-left">{product.price_regular}€</span>
                                        <span className="float-right">
                                            <MDBTooltip placement="top">
                                                <MDBBtn onClick={() => this.handleItemSelected(product)} tag="a" color="transparent" size="lg" className="p-1 m-0 mr-2 z-depth-0" >
                                                    <MDBIcon icon="shopping-cart" />
                                                </MDBBtn>
                                                <div>Add to Cart</div>
                                            </MDBTooltip>
                                            <MDBTooltip placement="top">
                                                <MDBBtn tag="a" href="https://mdbootstrap.com" target="_blank" color="transparent" size="lg" className="p-1 m-0 mr-2 z-depth-0" >
                                                    <MDBIcon icon="share-alt" />
                                                </MDBBtn>
                                                <div>Share</div>
                                            </MDBTooltip>
                                            <MDBTooltip placement="top">
                                                <MDBBtn tag="a" color="transparent" size="lg" className="p-1 m-0 z-depth-0" >
                                                    <MDBIcon icon="heart" className="red-text" />
                                                </MDBBtn>
                                                <div>Added to Wishlist</div>
                                            </MDBTooltip>
                                        </span>
                                    </MDBCardFooter>
                                </MDBCardBody>
                            </MDBCard>
                        )
                    })
                }
            </MDBRow>
        )
    }
}

const mapStateToProps = (state /**, ownProps */) => {
    return {
        error: getProductsError(state.products),
        pending: getProductsStatus(state.products),
        products: getProducts(state.products)
    }
}

const mapDispatchToProps = dispatch => bindActionCreators({
    getProducts: store_product.getProducts,
    getProductById: store_product.getProductById,
    addItem: store_cart.addItem,
    reoveItem: store_cart.reoveItem,
}, dispatch)

export default connect(
    mapStateToProps,  //subscribing to the store by mapping store values in our component's props || null
    mapDispatchToProps //inject action creators in our component's props
)(ListProduct)
