/** ✅ Husehold import ✅ **/
import React from 'react'
import { MDBAnimation, MDBCard, MDBCardBody, MDBCardHeader, MDBBadge, MDBBtn, MDBDataTable } from 'mdbreact';

/** 💰 Store 💰 **/
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import store from "../../redux/products/products.store"
import { getProducts, getProductsError, getProductsStatus } from '../../redux/products/products.reducer'

class ListProduct extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
        }
    }
    componentWillMount = () => {
        // if app product state is empty fetch, otherwise print use cache
        !this.props.products.length ? this.props.getProducts() : console.log("Cached products")
    }
    updateList = () => {
        this.props.getProducts()
    }
    handleRowClick = (row) => {
        //console.log(row)
    }
    render() {
        const { products, pending } = this.props
        const data_panel = {
            columns: [
                {
                    'label': 'Name',
                    'field': 'name',
                    'sort': 'asc',
                    width: 150
                },
                {
                    'label': 'Stock Q.ty',
                    'field': 'stock_qty',
                    'sort': 'asc',
                    width: 120
                },
                {
                    'label': 'Unit Sold',
                    'field': 'stock_sold',
                    'sort': 'asc',
                    width: 120
                },
                {
                    'label': 'Status',
                    'field': 'status',
                    'sort': 'asc',
                    width: 90
                }
            ],
            rows: this.props.products.map(item => {
                return {
                    name: item.name,
                    stock_qty: item.stock_qty,
                    stock_sold: item.stock_sold,
                    status: item.is_active ? <MDBBadge color="success">Available</MDBBadge> : <MDBBadge color="danger">Retired</MDBBadge>,
                    clickEvent: () => this.handleRowClick(item)
                }
            })
        }
        return (
            <MDBCard narrow>
                <MDBCardHeader className="view view-cascade gradient-card-header blue-gradient d-flex justify-content-between align-items-center py-2 mx-4 mb-3">
                    <div>
                        <MDBBtn outline rounded size="sm" color="white" className="px-2">
                            <i className="fa fa-th-large mt-0"></i>
                        </MDBBtn>
                        <MDBBtn outline rounded size="sm" color="white" className="px-2">
                            <i className="fa fa-columns mt-0"></i>
                        </MDBBtn>
                    </div>
                    <a href="#" className="white-text mx-3">Products</a>
                    <div>
                        <MDBBtn onClick={() => this.props.history.push("/dashboard/product/add")} outline rounded size="sm" color="white" className="px-2">
                            <i className="fas fa-plus mt-0"></i>
                        </MDBBtn>
                        <MDBBtn onClick={this.updateList} outline rounded size="sm" color="white" className="px-2">
                            {pending ?
                                <MDBAnimation infinite type="flash">
                                    <i className="fa fa-arrow-down mt-0"></i>
                                </MDBAnimation>
                                :
                                <i className="fa fa-sync mt-0"></i>
                            }
                        </MDBBtn>


                    </div>
                </MDBCardHeader>
                <MDBCardBody cascade>
                    <MDBDataTable className="custom-data-table" data={data_panel} scrollX>
                    </MDBDataTable>
                </MDBCardBody>
            </MDBCard >
        )
    }
}

const mapStateToProps = (state /**, ownProps */) => {
    return {
        error: getProductsError(state.products),
        pending: getProductsStatus(state.products),
        products: getProducts(state.products)
    }
}

const mapDispatchToProps = dispatch => bindActionCreators({
    getProducts: store.getProducts,
    getProductById: store.getProductById
}, dispatch)

export default connect(
    mapStateToProps,  //subscribing to the store by mapping store values in our component's props || null
    mapDispatchToProps //inject action creators in our component's props
)(ListProduct)
