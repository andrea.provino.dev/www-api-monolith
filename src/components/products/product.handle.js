/** ✅ Husehold import ✅ **/
import React from 'react'
// import { MDBCard, MDBCardImage, MDBIcon, MDBRow, MDBCol, } from 'mdbreact';
import { MDBAlert, MDBRow, MDBCol, MDBInput, MDBCard, MDBCardBody, MDBFileInput, MDBCardImage, MDBBadge, MDBModalFooter, MDBIcon, MDBBtn } from
    "mdbreact";

/** 💰 Store 💰 **/
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import store from "../../redux/products/products.store"
import { getProducts, getProductsError, getProductsStatus } from '../../redux/products/products.reducer'

import https from "../../utils/axios.config"

const handle_add_state = {
    title: "Add new Product",
    button_text: "Add product"
}
const form_empty = {
    stock_qty: 0,
    stock_sold: 0,
    price_discounted: 0,
    price_regular: 0,
    cost: 0,
    is_active: true,
    description: "",
    pictures: [],
    files: [],
    name: "",
}
class ProductHandle extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            form_value: Object.assign({}, form_empty),
            form_config: Object.assign({}, handle_add_state),
            form_errors: []
        }
        this.handleStatusChange = this.handleStatusChange.bind(this)
        this.handleInputChange = this.handleInputChange.bind(this)
        this.handleFileChange = this.handleFileChange.bind(this)
    }
    handleStatusChange = () => {
        const new_form = this.state.form_value;
        new_form.is_active = !this.state.form_value.is_active;
        this.setState({
            ...this.state,
            form_value: new_form
        })
    }
    handleInputChange = (e) => {
        const field = e.target.name;
        const value = e.target.value;
        const new_form = this.state.form_value
        new_form[field] = value
        this.setState({
            ...this.state,
            form_value: new_form
        })
    }
    handleFileChange = (files) => {
        const new_form = this.state.form_value
        new_form.files = files
        this.setState({
            ...this.state,
            form_value: new_form
        })
    }
    checkFormError = () => {
        const form = this.state.form_value
        let errors = []
        let error = undefined
        Object.keys(form).forEach((key) => {
            switch (key) {
                case 'price_regular':
                    error = form[key] === 0 ? "Public price can not be 0" : undefined
                    break;
                case 'name':
                    error = form[key] === "" ? "Product must have a name" : undefined
                    break;
                case 'description':
                    error = form[key] === "" ? "Product must have a description" : undefined
                    break;
                case 'price_discounted':
                    error = (form[key] >= form.price_regular && form[key] !== 0) ? "Public price can not be lower or equal than discounted price" : undefined
                    break;
                case 'stock_qty':
                    error = form[key] === 0 ? "Product qty must be higher than 0" : undefined
                    break;
                default:
                    error = undefined
            }
            if (error) errors.push(error)
        })
        return errors;
    }
    handleSubmit = async () => {
        const errors = this.checkFormError()
        if (!errors.length) {
            const form = this.state.form_value
            // STEP 1: upload images and get url back
            if (form.files.length) {
                const payload_images = new FormData()
                for (var x = 0; x < form.files.length; x++) {
                    payload_images.append('file', form.files[x])
                }
                await https.post("/uploadfile", payload_images)
                    .then(res => { // then print response status
                        form.pictures = res.data.file.locations
                    })
                    .catch(err => {
                        console.log(err)
                    })
            }
            // copy object
            let payload = Object.assign({}, form);
            // delete unwanted key
            delete payload.files
                ;
            await https.post("/products", payload)
                .then(res => { // then print response status
                    //console.log(res)
                        ;
                })
                .catch(err => {
                    console.log(err)
                })
            // STEP 2: save new product
            this.setState({
                ...this.state,
                form_errors: [],
                form_value: form_empty
            })
        } else {
            this.setState({
                ...this.state,
                form_errors: errors
            })
        }
    }
    render() {
        const { products, pending } = this.props
        const form = this.state.form_config
        return (
            <React.Fragment>
                <MDBCard>
                    <MDBCardBody className="mx-4">
                        <div className="text-center">
                            <h3 className="dark-grey-text mb-5">
                                <strong>{form.title}</strong>
                            </h3>
                        </div>
                        <MDBRow>
                            <MDBCol md="6">
                                <MDBInput
                                    label="Name"
                                    group
                                    name="name"
                                    onChange={this.handleInputChange}
                                    value={this.state.form_value.name}
                                    type="text"
                                />
                            </MDBCol>
                            <MDBCol md="3">
                                <MDBInput
                                    label="Cost"
                                    name="cost"
                                    onChange={this.handleInputChange}
                                    value={this.state.form_value.cost}
                                    group
                                    type="number"
                                />
                            </MDBCol>
                            <MDBCol md="3">
                                <MDBInput
                                    label="Public Price"
                                    name="price_regular"
                                    onChange={this.handleInputChange}
                                    value={this.state.form_value.price_regular}
                                    group
                                    type="number"
                                />
                            </MDBCol>
                        </MDBRow>
                        <MDBRow>
                            <MDBCol md="6">
                                <MDBInput
                                    label="Description"
                                    name="description"
                                    onChange={this.handleInputChange}
                                    value={this.state.form_value.description}
                                    group
                                    type="text"
                                />
                            </MDBCol>
                            <MDBCol md="3" >
                                <MDBInput
                                    label="Stock Qty"
                                    name="stock_qty"
                                    onChange={this.handleInputChange}
                                    value={this.state.form_value.stock_qty}
                                    group
                                    type="text"
                                />
                            </MDBCol>
                            <MDBCol md="3">
                                <MDBInput
                                    label="Stock Sold"
                                    name="stock_sold"
                                    onChange={this.handleInputChange}
                                    value={this.state.form_value.stock_sold}
                                    group
                                    type="text"
                                />
                            </MDBCol>
                        </MDBRow>
                        <MDBRow>

                            <MDBCol md="3" className="d-flex justify-content-center align-items-center offset-md-6">
                                {this.state.form_value.is_active ?
                                    <MDBBadge className="pointer p-2" onClick={this.handleStatusChange} color="success">Available</MDBBadge>
                                    :
                                    <MDBBadge className="pointer p-2" onClick={this.handleStatusChange} color="danger">Retired</MDBBadge>
                                }
                            </MDBCol>
                            <MDBCol md="3">
                                <MDBInput
                                    label="Discounted Price"
                                    name="price_discounted"
                                    onChange={this.handleInputChange}
                                    value={this.state.form_value.price_discounted}
                                    group
                                    type="number"
                                />
                            </MDBCol>
                        </MDBRow>
                        <MDBRow>
                            <MDBCol md="11">
                                <MDBFileInput
                                    btnTitle="Select images"
                                    multiple
                                    textFieldTitle="Upload images"
                                    className="product-form-upload-file"
                                    getValue={this.handleFileChange}
                                />
                            </MDBCol>
                            <MDBCol md="1" className="d-flex justify-content-center align-items-center">
                                <MDBBadge className="p-2" color="warning">{this.state.form_value.files.length} files</MDBBadge>
                            </MDBCol>

                        </MDBRow>
                        <div className="text-center mb-3">
                            <MDBBtn
                                onClick={this.handleSubmit}
                                type="button"
                                gradient="blue"
                                rounded
                                className="btn z-depth-1a"
                            >{form.button_text}
                            </MDBBtn>
                        </div>
                    </MDBCardBody>
                    <MDBModalFooter className="mx-5 pt-3 mb-1">
                        {(this.state.form_errors.length > 0) &&
                            <MDBAlert className="w-100" color="warning" >{
                                this.state.form_errors.map(item => {
                                    return (
                                        <p className="my-0">{item}</p>
                                    )
                                })}
                            </MDBAlert>
                        }
                    </MDBModalFooter>
                </MDBCard>
            </React.Fragment>
        )
    }
}

const mapStateToProps = (state /**, ownProps */) => {
    return {
        error: getProductsError(state.products),
        pending: getProductsStatus(state.products),
        products: getProducts(state.products)
    }
}

const mapDispatchToProps = dispatch => bindActionCreators({
    getProducts: store.getProducts,
    getProductById: store.getProductById
}, dispatch)

export default connect(
    mapStateToProps,  //subscribing to the store by mapping store values in our component's props || null
    mapDispatchToProps //inject action creators in our component's props
)(ProductHandle)
