/** ✅ Husehold import ✅ **/
import React from "react";
import { MDBInput, MDBContainer, MDBModal, MDBModalBody, MDBModalHeader, MDBModalFooter, MDBCard, MDBCardBody, MDBCardHeader, MDBBadge, MDBBtn, MDBDataTable } from 'mdbreact';

/** 💰 Store 💰 **/
import { bindActionCreators } from "redux"
import { connect } from "react-redux"


/** import users from store **/
import { getUsers } from "../../redux/users/users.store"
import { getUsersError, getUsersStatus, getUsersList } from "../../redux/users/users.reducer"

// import css
import "../sidebar/sidebar.css"

//other imports
import moment from "moment"

class UserList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            info_item_open: false,
            user_selected: {}
        }
        this.handleDateTime = this.handleDateTime.bind(this)
    }
    componentDidMount = () => {
        this.props.getUsers()
    }
    toggle = modal_name => {
        ;
        this.setState({
            ...this.state,
            [modal_name]: !this.state[modal_name]
        })
    }
    handleRowClick = (row) => {
        this.setState({
            ...this.state,
            user_selected: row,
            info_item_open: true
        })
    }
    getUserStatus = (status) => {
        return status ? <MDBBadge color="success">Active</MDBBadge> : <MDBBadge color="danger">Deleted</MDBBadge>
    }
    handleDateTime = dateTime => () => {
        ;
        return dateTime ? moment(dateTime).format("DD/MM/YY HH:mm") : "Not Available"
    }
    render() {
        const data_panel = {
            columns: [
                {
                    'label': 'Fullname',
                    'field': 'fullname',
                    'sort': 'asc',
                    width: 150
                },
                {
                    'label': 'Email',
                    'field': 'email',
                    'sort': 'asc',
                    width: 150
                },
                {
                    'label': 'Member Since',
                    'field': 'created_on',
                    'sort': 'asc',
                    width: 120
                },
                {
                    'label': 'Status',
                    'field': 'status',
                    'sort': 'asc',
                    width: 120
                },
                {
                    'label': 'Last Access',
                    'field': 'last_access',
                    'sort': 'asc',
                    width: 150
                },
            ],
            rows: this.props.users.map(item => {
                return {
                    fullname: (item.firstname || "") + " " + (item.lastname || ""),
                    email: item.email,
                    created_on: item.created_on ? moment(item.created_on).format("DD/MM/YY HH:mm") : "Not Available",
                    active: this.getUserStatus(item.is_active),
                    last_access: item.last_access ? moment(item.last_access).format("DD/MM/YY HH:mm") : "Not Available",
                    clickEvent: () => this.handleRowClick(item)
                }

            })
        };
        const { user_selected } = this.state
        return (
            <React.Fragment>
                <MDBContainer>
                    <MDBModal isOpen={this.state.info_item_open} toggle={() => this.toggle("info_item_open")} fullHeight position="right">
                        <MDBModalHeader toggle={() => this.toggle("info_item_open")}>User Detail</MDBModalHeader>
                        <MDBModalBody>
                            {this.getUserStatus(user_selected.is_active)}
                            <MDBInput label="User" disabled value={user_selected.fullname || (user_selected.firstname + " " + user_selected.lastname)} />
                            <MDBInput label="User Email" disabled value={user_selected.email} />
                            <MDBInput label="User Phone Number" disabled value={user_selected.phone_number} />
                            <MDBInput label="Member Since" disabled value={this.handleDateTime(user_selected.created_on)} />
                            <MDBInput label="Last Access" disabled value={this.handleDateTime(user_selected.last_access)} />
                            <MDBInput label="User ID" disabled value={user_selected._id} />
                        </MDBModalBody>
                        <MDBModalFooter>
                            <MDBBtn color="info" onClick={() => this.toggle("info_item_open")}>Close</MDBBtn>
                            <MDBBtn disabled color="success">Save changes</MDBBtn>
                        </MDBModalFooter>
                    </MDBModal>
                </MDBContainer>
                <MDBCard narrow>
                    <MDBCardHeader className="view view-cascade gradient-card-header blue-gradient d-flex justify-content-between align-items-center py-2 mx-4 mb-3">
                        <div>
                            <MDBBtn outline rounded size="sm" color="white" className="px-2">
                                <i className="fa fa-th-large mt-0"></i>
                            </MDBBtn>
                            <MDBBtn outline rounded size="sm" color="white" className="px-2">
                                <i className="fa fa-columns mt-0"></i>
                            </MDBBtn>
                        </div>
                        <a href="#" className="white-text mx-3">Users</a>
                        <div>
                            <MDBBtn outline rounded size="sm" color="white" className="px-2">
                                <i className="fas fa-pencil-alt mt-0"></i>
                            </MDBBtn>
                            <MDBBtn outline rounded size="sm" color="white" className="px-2">
                                <i className="fas fa-times mt-0"></i>
                            </MDBBtn>
                            <MDBBtn outline rounded size="sm" color="white" className="px-2">
                                <i className="fa fa-info-circle mt-0"></i>
                            </MDBBtn>
                        </div>
                    </MDBCardHeader>
                    <MDBCardBody cascade>
                        <MDBDataTable className="custom-data-table" data={data_panel} scrollX />
                    </MDBCardBody>
                </MDBCard>

            </React.Fragment>
        )
    }


}

const mapStateToProps = (state) => {
    return {
        users: getUsersList(state.users),
        pending: getUsersStatus(state.users),
        error: getUsersError(state.users)
    }
}
const mapDispatchToProps = dispatch => bindActionCreators({
    getUsers: getUsers

}, dispatch)

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(UserList)